//---------------------------------------------------------------------------
//!
//!	@file	Lib.Camera.h
//!	@brief	カメラ関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace Camera
	{
		//===========================================================================
		//!	カメラクラス
		//===========================================================================
		class Camera
		{
		public:
			//!	コンストラクタ
			Camera(void);
			//!	デストラクタ
			~Camera(void);

			//!	初期化
			void	Init(void);
			//!	更新
			void	Update(void);

			//!	プロジェクションのセット
			void	SetProjection(float fFovY, float fZNear, float fZFar);

			//!	位置のセット
			void	SetPos(const Vector3& Pos);
			//!	位置のセット
			void	SetTarget(const Vector3& Target);
			//!	カメラの上を指すベクトルのセット
			void	SetUp(const Vector3& Up);
			//!	位置のセット
			Vector3	GetPos(void);
			//!	位置のセット
			Vector3	GetTarget(void);

			//!	ビュー変換行列の取得
			Matrix	GetView(void);
			//! 射影系変換行列の取得
			Matrix	GetProjection(void);

		private:
			Matrix	_View;			//!< ビュー変換行列
			Matrix	_Projection;	//!< 射影変換行列
			Vector3	_Pos;			//!< 位置
			Vector3 _Target;		//!< ターゲット
			Vector3 _Up;			//!< 上を指すベクトル

			float	_fFovY;			//!< 視野角
			float	_fAspect;		//!< アスペクト比
			float	_fZNear;		//!< 前方クリップ面
			float	_fZFar;			//!< 後方クリップ面
		};
	}
}

//============================================================================
//	END OF FILE
//============================================================================