//---------------------------------------------------------------------------
//
//	2D.fx
//	エフェクトファイル
//
//---------------------------------------------------------------------------

//===========================================================================
//	テクスチャー
//===========================================================================
//	環境マップ
texture DiffuseMap;
sampler DiffuseSamp = sampler_state
{
	Texture		= <DiffuseMap>;
	MinFilter	= LINEAR;
	MagFilter	= LINEAR;
	MipFilter	= NONE;

	AddressU	= Wrap;
	AddressV	= Wrap;
};

//===========================================================================
//	ピクセルシェーダー
//===========================================================================
// 基本
float4	PS_Basic( float4 Color : COLOR0, float2 Tex : TEXCOORD0 ) : COLOR
{
	return Color * tex2D(DiffuseSamp, Tex);
}

//===========================================================================
//	テクニック
//===========================================================================
// スプライト
technique Basic
{
	pass P0	// 線形合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		PixelShader	= compile ps_2_0 PS_Basic();
	}
	pass P1	// 加算合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = true;
		
		PixelShader	= compile ps_2_0 PS_Basic();
	}
	pass P2	// 減算合成
	{
		AlphaBlendEnable = true;
		BlendOp          = RevSubtract;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = true;
		
		PixelShader	= compile ps_2_0 PS_Basic();
	}
	pass P3 // 乗算合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = Zero;
		DestBlend        = SrcColor;
		ZWriteEnable     = true;
		
		PixelShader	= compile ps_2_0 PS_Basic();
	}
}

//============================================================================
//	END OF FILE
//============================================================================