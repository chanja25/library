//---------------------------------------------------------------------------
//!
//!	@file	Lib.System.Window.h
//!	@brief	ウィンドウ関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.System.h"

namespace Lib
{
	namespace System
	{
		namespace Window
		{
			//===========================================================================
			//!	ウィンドウクラス
			//===========================================================================
			class Window
			{
			public:
				//!	コンストラクタ
				Window(void);
				//!	コンストラクタ
				Window(int iWidth, int iHeight);
				//!	デストラクタ
				~Window(void);

				//!	初期化
				bool	Init(const char* pWindowName, bool bFullScreen = false, bool bMultiple = false);
				//!	解放
				void	Cleanup(void);

				//!	ウィンドウハンドルの取得
				HWND	GetWindowHandle(void);
				//!	画面横サイズの取得
				int		GetWidth(void);
				//!	画面縦サイズの取得
				int		GetHeight(void);
				//!	フルスクリーンフラグの取得
				bool	GetFullScreen(void);

				//!	ウィンドウプロシージャーの設定
				void	SetWindowProc(WNDPROC WndProc);

			private:
				HWND	_hWnd;			//!< ウィンドウハンドル
				int		_iWidth;		//!< 画面横サイズ
				int		_iHeight;		//!< 画面縦サイズ
				bool	_bFullScreen;	//!< フルスクリーンフラグ
				WNDPROC	_WndProc;		//!< ウィンドウプロシージャー
			};
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================