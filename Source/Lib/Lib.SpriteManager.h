//---------------------------------------------------------------------------
//!
//!	@file	Lib.SpriteManager.h
//!	@brief	スプライト管理
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace SpriteManager
	{
		//===========================================================================
		//!	スプライトクラス
		//===========================================================================
		class Sprite
		{
		public:
			//!	コンストラクタ
			Sprite(void);
			//!	コンストラクタ
			Sprite(const char* pFileName);
			//!	デストラクタ
			~Sprite(void);

			//!	読み込み
			bool	Load(const char* pFileName);
			//!	スプライトの生成
			bool	Create(dword dwWidth, dword dwHeight, int iFormat = ETextureFormat::MANAGED);
			//!	描画
			void	Render(Shader::Shader* pShader, char* pTechnique, dword dwPass = 0);
			//!	描画
			void	Render(void);

			//!	位置、サイズのセット
			void	SetRect(float fX, float fY, float fWidth, float fHeight);
			//!	位置のセット
			void	SetPosition(float fX, float fY);
			//!	サイズの取得
			void	SetSize(float fWidth, float fHeight);

			//!	位置、サイズの取得
			Vector4	GetRect(void);
			//!	位置の取得
			Vector2	GetPosition(void);
			//!	サイズの取得
			Vector2	GetSize(void);

			//!	画像内の位置、サイズのセット
			void	SetSrcRect(float fX, float fY, float fWidth, float fHeight);
			//!	画像内の位置のセット
			void	SetSrcPosition(float fX, float fY);
			//!	画像内のサイズのセット
			void	SetSrcSize(float fWidth, float fHeight);

			//!	位置、サイズの取得
			Vector4	GetSrcRect(void);
			//!	画像内の位置の取得
			Vector2	GetSrcPosition(void);
			//!	画像内のサイズの取得
			Vector2	GetSrcSize(void);

			//!	色のセット
			void	SetColor(dword dwColor = 0xffffffff);
			//!	テクスチャーの取得
			LPDIRECT3DTEXTURE9	GetTexture(void);

		private:
			LPDIRECT3DTEXTURE9	_pTexture;	//!< テクスチャー

			Vector4	_DestRect;	//!< 表示の位置、サイズ
			Vector4	_SrcRect;	//!< 画像内の位置、サイズ

			dword	_dwColor;	//!< 色
			float	_fWidth;	//!< 横幅
			float	_fHeight;	//!< 縦幅
		};
	}
}

//============================================================================
//	END OF FILE
//============================================================================