//---------------------------------------------------------------------------
//!
//!	@file	App.h
//!	@brief	アプリケーション
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

// --------------------------------------------------------------------------
//	include
// --------------------------------------------------------------------------
#include "../Lib/Lib.h"

#include "App.System.h"
#include "App.Input.h"
#include "App.Collision.h"
#include "App.Game.Character.h"
#include "App.Game.Player.h"
#include "App.Scene.h"

namespace App
{
#ifdef	_DEBUG
	extern Lib::FontManager::Font* pDebugFont;
#endif	// ~#if _DEBUG

	//	シーン管理クラス
	extern Lib::SceneManager::SceneManager* pSceneManager;
	//	インプット
	extern App::Input::Input* pInput;
	//	当たり判定
	extern App::Collision::Collision* pCollision;

	//!	シーン
	namespace ESceneMode
	{
		const int TEST = -1;
		const int TITLE = 0;
		const int GAME = 1;
		const int CREAR = 255;
	}

	//!	キーコード
	namespace EKeyCode
	{
		const int CROSS = 0;
		const int CIRCLE = 1;
		const int SQUARE = 2;
		const int TRIANGLE = 3;
		const int L1 = 4;
		const int R1 = 5;
		const int L2 = 6;
		const int R2 = 7;
		const int SELECT = 8;
		const int START = 9;
		const int UP = 11;
		const int DOWN = 12;
		const int LEFT = 13;
		const int RIGHT = 14;
	}

	//!	プレイヤー
	namespace EPlayer
	{
		//!	プレイヤーステート
		namespace EState
		{
			const int WAIT = 0;
			const int MOVE = 1;
			const int JUMP_BEGIN = 2;
			const int JUMP = 3;
			const int JUMP_END = 4;
		}

		//!	プレイヤーモーション
		namespace EMotion
		{
			const int WAIT = 0;
			const int WALK = 1;
			const int RUN = 2;
			const int JUMP_BEGIN = 3;
			const int JUMP = 4;
			const int JUMP_END = 5;
		}
	}

	//!	敵
	namespace EEnemy
	{
		//!	プレイヤーステート
		namespace EState
		{
			const int WAIT = 0;
			const int MOVE = 1;
			const int JUMP_BEGIN = 2;
			const int JUMP = 3;
			const int JUMP_END = 4;
		}

		//!	プレイヤーモーション
		namespace EMotion
		{
			const int WAIT = 0;
			const int WALK = 1;
			const int RUN = 2;
			const int JUMP_BEGIN = 3;
			const int JUMP = 4;
			const int JUMP_END = 5;
		}
	}

	//!	当たり判定
	namespace ECollision
	{
		//!	形
		namespace EShape
		{
			const int SPHERE = 0;
			const int CAPSULE = 1;
		}
		//!	属性
		namespace EAttribute
		{
			const int NONE = 0;
			const int PLAYER = (1 << 0);
			const int ENEMY = (1 << 1);
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================