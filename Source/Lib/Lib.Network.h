//---------------------------------------------------------------------------
//!
//!	@file	Lib.Network.h
//!	@brief	通信関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace Network
	{
		//===========================================================================
		//!	サーバーベース
		//===========================================================================
		class ServerBase
		{
		public:
			//!	コンストラクタ
			ServerBase(void) {}
			//!	デストラクタ
			virtual ~ServerBase(void) {}

			//!	接続待ち開始
			virtual int		Accept(int iMicroSecond) = 0;
			//!	接続を切る
			virtual	bool	CloseClient(int iNo) = 0;

			//!	全員に送信
			virtual void	Send(void* pData, int iSize) = 0;
			//!	指定した相手に送信
			virtual bool	Send(int iNo, void* pData, int iSize) = 0;
			//!	受信
			virtual int		Receive(void* pData, int* pSize) = 0;
			//!	指定した相手から受信
			virtual int		Receive(int iNo, void* pData, int iSize) = 0;

			//!	クライアント数の取得
			virtual int		GetClientNum(void) = 0;
		};

		//===========================================================================
		//!	クライアントベース
		//===========================================================================
		class ClientBase
		{
		public:
			//!	コンストラクタ
			ClientBase(void) {}
			//!	デストラクタ
			virtual ~ClientBase(void) {}

			//!	全員に送信
			virtual bool	Send(void* pData, int iSize) = 0;
			//!	受信
			virtual int		Receive(void* pData, int iSize) = 0;
		};

		//!	サーバークラス生成
		bool	Create(ServerBase** ppServer, int iPort, int iClientMax);
		//!	サーバークラス解放
		void	Delete(ServerBase** ppServer);

		//!	クライアントクラス生成
		bool	Create(ClientBase** ppClient, char* pIpAddress, int iPort);
		//!	クライアントクラス解放
		void	Delete(ClientBase** ppClient);
	}
}

//============================================================================
//	END OF FILE
//============================================================================