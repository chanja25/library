//---------------------------------------------------------------------------
//!
//!	@file	App.System.cpp
//!	@brief	アプリケーションシステム
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//	include
//---------------------------------------------------------------------------
#include "App.h"

namespace App
{
#if	_DEBUG
	//!	デバッグフォント
	Lib::FontManager::Font* pDebugFont = null;
#endif	// ~#if _DEBUG

	//!	シーン管理
	Lib::SceneManager::SceneManager* pSceneManager = null;
	//!	インプット
	App::Input::Input* pInput = null;
	//!	当たり判定
	App::Collision::Collision* pCollision = null;

	namespace System
	{
		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		AppSystem::AppSystem(void)
		{
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		AppSystem::~AppSystem(void)
		{
		}

		//---------------------------------------------------------------------------
		//	初期化
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	AppSystem::Init(void)
		{
#if	_DEBUG
			pDebugFont = new Lib::FontManager::Font;
			pDebugFont->Init(24, 400, false, "MS明朝");
#endif	// ~#if _DEBUG

			//	インプット生成
			pInput = new App::Input::Input;
			pInput->Init();

			//	当たり判定
			pCollision = new App::Collision::Collision;

			//	シーン管理クラス生成
			pSceneManager = new Lib::SceneManager::SceneManager;

			//---------------------------------------------------------------------------
			//	シーンの設定
			//---------------------------------------------------------------------------
			{
				Lib::SceneManager::SceneBase* pScene = null;

				//	テスト
				pScene = new Scene::SceneTest;
				Lib::SceneManager::SetScene(ESceneMode::TEST, pScene, true);

				//	タイトル
				pScene = new Scene::SceneTitle;
				Lib::SceneManager::SetScene(ESceneMode::TITLE, pScene, false);

				//	ゲーム
				pScene = new Scene::SceneGame;
				Lib::SceneManager::SetScene(ESceneMode::GAME, pScene, true);
			}

			//	初期シーン設定
			pSceneManager->JumpScene(ESceneMode::TEST);

			return true;
		}

		//---------------------------------------------------------------------------
		//	解放
		//---------------------------------------------------------------------------
		void	AppSystem::Cleanup(void)
		{
			Lib::SafeDelete(pInput);
			Lib::SafeDelete(pCollision);
			Lib::SafeDelete(pSceneManager);

#if	_DEBUG
			Lib::SafeDelete(pDebugFont);
#endif	// ~#if _DEBUG
		}

		//---------------------------------------------------------------------------
		//	更新
		//!	@param dElapsedTime [in] 経過時間
		//---------------------------------------------------------------------------
		void	AppSystem::Update(double dElapsedTime)
		{
			pSceneManager->Update(dElapsedTime);

			pCollision->Update();

#if	_DEBUG
			//	FPS計測
			_FPSCounter.Update(dElapsedTime);
#endif	// ~#if _DEBUG
		}

		//---------------------------------------------------------------------------
		//	更新
		//!	@param dElapsedTime [in] 経過時間
		//---------------------------------------------------------------------------
		void	AppSystem::Render(double dElapsedTime)
		{
			_pDirectX->Clear(0xff00aaaa);

			pSceneManager->Render(dElapsedTime);

#ifdef _DEBUG
			pDebugFont->Draw(0, 720, Lib::CharSet::Format("×:%d ○:%d □:%d △%d\nL1:%d R1:%d L2:%d R2:%d\nSELECT:%d START:%d\n↑:%d ↓:%d ←:%d →:%d",
									pInput->IsKeyPress(EKeyCode::CROSS),
									pInput->IsKeyPress(EKeyCode::CIRCLE),
									pInput->IsKeyPress(EKeyCode::SQUARE),
									pInput->IsKeyPress(EKeyCode::TRIANGLE),
									pInput->IsKeyPress(EKeyCode::L1),
									pInput->IsKeyPress(EKeyCode::R1),
									pInput->IsKeyPress(EKeyCode::L2),
									pInput->IsKeyPress(EKeyCode::R2),
									pInput->IsKeyPress(EKeyCode::SELECT),
									pInput->IsKeyPress(EKeyCode::START),
									pInput->IsKeyPress(EKeyCode::UP),
									pInput->IsKeyPress(EKeyCode::DOWN),
									pInput->IsKeyPress(EKeyCode::LEFT),
									pInput->IsKeyPress(EKeyCode::RIGHT)),
									0xff00ff00, Lib::EFormat::BOTTOM);

			pDebugFont->Draw(0, 0, Lib::CharSet::Format("FPS:%f", _FPSCounter.GetFPS()), 0xff00ff00);
			pDebugFont->Draw(1280, 24, "Enterで次シーンへ", 0xff00ff00, Lib::EFormat::RIGHT);
#endif	// ~#if _DEBUG
		}

		//---------------------------------------------------------------------------
		//	ウインドウプロシージャー
		//!	@param hWnd [in] ウィンドウハンドル
		//!	@param msg [in] ウィンドウメッセージ
		//!	@param wParam [in] パラメータ引数１
		//!	@param lParam [in] パラメータ引数２
		//!	@return 結果
		//---------------------------------------------------------------------------
		LRESULT	CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			switch( msg )
			{
			case WM_KEYDOWN:
				//	キー入力
				switch( wParam )
				{
				case VK_ESCAPE:
					//	ESCが押された時、終了処理をする
					PostMessage(hWnd, WM_CLOSE, 0, 0);
					return 0L;
				}
				break;
			case WM_DESTROY:
				//	ウインドウの破棄
				PostQuitMessage(0);
				return 0L;
			}

			//	デフォルトのウィンドウ処理
			return DefWindowProc(hWnd, msg, wParam, lParam);
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================