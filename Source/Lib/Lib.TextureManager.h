//---------------------------------------------------------------------------
//!
//!	@file	Lib.TextureManager.h
//!	@brief	テクスチャー管理
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace TextureManager
	{
		//!	テクスチャーの作成
		bool Create(LPDIRECT3DTEXTURE9* ppTexture, const char* pFileName);
		//!	テクスチャーの作成
		bool Create(LPDIRECT3DTEXTURE9* ppTexture, dword dwWidth, dword dwHeight, int iFormat = ETextureFormat::MANAGED);
		//!	テクスチャーの削除
		void Delete(LPDIRECT3DTEXTURE9* ppTexture);
	}
}