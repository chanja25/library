//---------------------------------------------------------------------------
//!
//!	@file	Lib.System.cpp
//!	@brief	システム
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//	include
//---------------------------------------------------------------------------
#include "Lib.h"

//---------------------------------------------------------------------------
// library
//---------------------------------------------------------------------------
#pragma comment(lib, "dxguid.lib")
#pragma	comment(lib, "d3d9.lib")
#pragma	comment(lib, "d3dx9.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dsound.lib")

#pragma comment(lib, "winmm.lib")

namespace Lib
{
	namespace InputManager
	{
		bool	Init(HWND hWnd);
		void	Cleanup();
		void	Update();
	}

	namespace SoundManager
	{
		bool	Init(HWND hWnd);
		void	Cleanup(void);
	}

	namespace TextureManager
	{
		void	Cleanup(void);
	}

	namespace MeshManager
	{
		void	Cleanup(void);
	}

	namespace SceneManager
	{
		void	Cleanup(void);
	}

	namespace System
	{
		//	インスタンスの生成
		System*	System::_pInstance = null;

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		System::System(void)
			: _pWindow(null), _pDirectX(null)
			, _pDevice(null), _pFrame(null)
			, _iWidth(1280), _iHeight(720)
			, _WndProc(null), _iExitCode(0)
		{
			Assert(_pInstance == null, "既にシステムが存在します");
			_pInstance = this;
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		System::~System(void)
		{
		}

		//---------------------------------------------------------------------------
		//	システム初期化
		//!	@param pName [in] ウィンドウ名
		//!	@param bFullScreen [in] フルスクリーンフラグ
		//!	@param bMultiple [in] 多重起動の可否
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	System::InitSystem(const char* pWindowName, bool bFullScreen, bool bMultiple)
		{
			//	ウィンドウの生成
			_pWindow = new Window::Window(_iWidth, _iHeight);
			_pWindow->SetWindowProc(_WndProc);
			if( !_pWindow->Init(pWindowName, bFullScreen, bMultiple) )
				return false;

			//	DirectXの初期化
			_pDirectX = new DirectX::DirectX;
			if( !_pDirectX->Init(_pWindow) )
				return false;

			//	Inputの初期化
			if( !InputManager::Init(_pWindow->GetWindowHandle()) )
				return false;

			//	サウンドの初期化
			if( !SoundManager::Init(_pWindow->GetWindowHandle()) )
				return false;

			//	フレームコントロールの生成
			_pFrame = new Frame::FrameControl;

			_pDevice = _pDirectX->GetDevice();
			_pDevice->AddRef();

			if( !Init() )
				return false;

			return true;
		}

		//---------------------------------------------------------------------------
		//	システム解放
		//---------------------------------------------------------------------------
		void	System::CleanupSystem(void)
		{
			Cleanup();

			SceneManager::Cleanup();
			MeshManager::Cleanup();
			TextureManager::Cleanup();
			SoundManager::Cleanup();
			InputManager::Cleanup();

			SafeRelease(_pDevice);
			SafeDelete(_pFrame);
			SafeDelete(_pDirectX);
			SafeDelete(_pWindow);
		}

		//---------------------------------------------------------------------------
		//	メインループ
		//---------------------------------------------------------------------------
		void	System::MainLoop(void)
		{
			MSG msg;

			while( true )
			{
				if( PeekMessage(&msg, null, 0, 0, PM_NOREMOVE) )
				{
					if( !GetMessage(&msg, null, 0, 0) )
						break;
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
				else
				{
					//	フレーム制御
					if( !_pFrame->Update() )
						continue;

					//	プロファイラーにフレームの開始を伝える
					START_PROFILE;

					InputManager::Update();

					BEGIN_PROFILE("System Update");
					//	更新
					Update(_pFrame->GetElapsedTime());

					END_PROFILE("System Update");

					//	現在描画可能か確認する
					if( _pFrame->UpdateRender() )
					{
						BEGIN_PROFILE("System Render");
						//	描画
						_pDirectX->BeginScene();
						Render(_pFrame->GetRenderElapsedTime());
						_pDirectX->EndScene();

						END_PROFILE("System Render");
					}
				}
			}

			_iExitCode = (int)msg.wParam;
		}

		//---------------------------------------------------------------------------
		//	ウィンドウプロシージャーの設定
		//!	@param WndProc [in] ウィンドウプロシージャー
		//---------------------------------------------------------------------------
		void	System::SetWindowProc(WNDPROC WndProc)
		{
			_WndProc = WndProc;
		}

		//---------------------------------------------------------------------------
		//	ウィンドウプロシージャーの設定
		//!	@param iWidth [in] 横サイズ
		//!	@param iHeight [in] 縦サイズ
		//---------------------------------------------------------------------------
		void	System::SetWindowSize(int iWidth, int iHeight)
		{
			_iWidth = iWidth;
			_iHeight = iHeight;
		}

		//---------------------------------------------------------------------------
		//	ウィンドウクラスの取得
		//!	@return ウィンドウクラス
		//---------------------------------------------------------------------------
		Window::Window*	System::GetWindow(void)
		{
			return _pWindow;
		}

		//---------------------------------------------------------------------------
		//	DirectXクラスの取得
		//!	@return DirectXクラス
		//---------------------------------------------------------------------------
		DirectX::DirectX*	System::GetDirectX(void)
		{
			return _pDirectX;
		}

		//---------------------------------------------------------------------------
		//	Direct3Dデバイスの取得
		//!	@return Direct3Dデバイス
		//---------------------------------------------------------------------------
		LPDIRECT3DDEVICE9	System::GetDevice(void)
		{
			return _pDevice;
		}

		//---------------------------------------------------------------------------
		//	終了コードの取得
		//!	@return 終了コード
		//---------------------------------------------------------------------------
		int		System::GetExitCode(void)
		{
			return _iExitCode;
		}

		//---------------------------------------------------------------------------
		//	インスタンスの取得
		//!	@return インスタンス
		//---------------------------------------------------------------------------
		System*	System::GetInstance(void)
		{
			return _pInstance;
		}

		//---------------------------------------------------------------------------
		//	ウインドウプロシージャー
		//!	@param hWnd [in] ウィンドウハンドル
		//!	@param msg [in] ウィンドウメッセージ
		//!	@param wParam [in] パラメータ引数１
		//!	@param lParam [in] パラメータ引数２
		//---------------------------------------------------------------------------
		LRESULT	CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			switch( msg )
			{
			case WM_KEYDOWN:
				//	キー入力
				switch( wParam )
				{
				case VK_ESCAPE:
					//	ESCが押された時、終了処理をする
					PostMessage(hWnd, WM_CLOSE, 0, 0);
					return 0L;
				}
				break;
			case WM_DESTROY:
				//	ウインドウの破棄
				PostQuitMessage(0);
				return 0L;
			}

			//	デフォルトのウィンドウ処理
			return DefWindowProc(hWnd, msg, wParam, lParam);
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================