//---------------------------------------------------------------------------
//!
//!	@file	Lib.FontManager.cpp
//!	@brief	フォント関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace FontManager
	{
		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Font::Font(void)
			: _pFont(null), _dwSize(0)
		{
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		Font::~Font(void)
		{
			SafeRelease(_pFont);
		}

		//---------------------------------------------------------------------------
		//	初期化
		//!	@param dwSize [in] サイズ
		//!	@param dwWeight [in] 太さ
		//!	@param bItalic [in] 斜体フラグ
		//!	@param pFont [in] 字体
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Font::Init(dword dwSize, dword dwWeight, bool bItalic, char* pFont)
		{
			SafeRelease(_pFont);

			//	フォントの生成
			if( FAILED(D3DXCreateFont(Lib::System::System::GetInstance()->GetDevice(),
									  dwSize,
									  0,
									  dwWeight,
									  0,
									  bItalic,
									  DEFAULT_CHARSET,
									  OUT_DEFAULT_PRECIS,
									  DEFAULT_QUALITY,
									  FIXED_PITCH | FF_DONTCARE,
									  pFont,
									  &_pFont)) )
			{
				Message("フォントの作成に失敗", "エラー");
				return false;
			}
			return true;
		}

		//---------------------------------------------------------------------------
		//	初期化
		//!	@param iX [in] X座標
		//!	@param iY [in] Y座標
		//!	@param pString [in] 文字列
		//!	@param dwColor [in] 色
		//!	@param dwFormat [in] フォーマット
		//---------------------------------------------------------------------------
		void	Font::Draw(int iX, int iY, const char* pString, dword dwColor, dword dwFormat)
		{
			static RECT Rect;
			Rect.left	= iX;
			Rect.right	= iX;
			Rect.top	= iY;
			Rect.bottom	= iY;

			if( dwFormat & DT_RIGHT )
				Rect.left -= _dwSize * strlen(pString);
			else
				Rect.right += _dwSize * strlen(pString);

			if( dwFormat & DT_BOTTOM )
				Rect.top -= _dwSize * strlen(pString);
			else
				Rect.bottom += _dwSize * strlen(pString);

			dwFormat = DT_NOCLIP | dwFormat;

			_pFont->DrawText(null,
							 pString,
							 -1,
							 &Rect,
							 dwFormat,
							 dwColor);
		}

		//---------------------------------------------------------------------------
		//	フォントサイズの取得
		//!	@return サイズ
		//---------------------------------------------------------------------------
		dword	Font::GetSize(void)
		{
			return _dwSize;
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================