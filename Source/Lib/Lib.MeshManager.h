//---------------------------------------------------------------------------
//!
//!	@file	Lib.MeshManager.h
//!	@brief	メッシュ関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace MeshManager
	{
		//===========================================================================
		//!	メッシュクラス
		//===========================================================================
		class Mesh
		{
		public:
			//!	コンストラクタ
			Mesh(void);
			//!	コンストラクタ
			Mesh(const char* pFileName);
			//!	デストラクタ
			virtual ~Mesh(void);

			//!	読み込み
			virtual bool	Load(const char* pFileName);

			//!	描画
			virtual void	Render(Shader::Shader* pShader, char* pTechnique, dword dwPass = 0);
			//!	描画
			virtual void	Render(dword i);

			//!	UVアニメーション
			void	UVAnimation(float fU, float fV);
			//!	レイピック
			int		RayPick(Vector3* pOut, const Vector3& Pos, Vector3* pVec, float* pDist, const Matrix& TransMatrix);

			//!	メッシュの取得
			LPD3DXMESH	GetMesh(void);
			//!	転送行列のセット
			void	SetTransMatrix(const Matrix& TransMatrix);
			//!	転送行列の取得
			Matrix	GetTransMatrix(void);
			//!	材質数の取得
			dword	GetMaterialNum(void);
			//!	材質の取得
			Material*	GetMaterial(void);

		protected:
			LPD3DXMESH	_pMesh;			//!< メッシュ
			Matrix		_TransMatrix;	//!< 転送行列

			dword		_dwMaterialNum;	//!< 材質数
			Material*	_pMaterial;		//!< 材質
		};

		//===========================================================================
		//!	スキンメッシュクラス
		//===========================================================================
		class SkinMesh
			: public Mesh
		{
		public:
			//!	コンストラクタ
			SkinMesh(void);
			//!	コンストラクタ
			SkinMesh(const char* pFileName);
			//!	デストラクタ
			~SkinMesh(void);

			//!	読み込み
			bool	Load(const char* pFileName);

			//!	更新
			void	Update(void);
			//!	更新
			void	Update(float fFrame);
			//!	描画
			void	Render(Shader::Shader* pShader, char* pTechnique, dword dwPass = 0);
			//!	描画
			void	Render(dword i);

			//!	モーションの変更
			void	SetMotion(word wMotion, bool bLoop = false, float fFrame = 0);
			//!	モーションブレンド
			void	SetMotionBlend(word wMotion1, word wMotion2, float fPower = -1, bool bLoop = false, float fFrame = 0);
			//!	フレームのリセット
			void	Reset(void);
			//!	モーションが終了しているか確認
			bool	IsEnd(void);

			//!	ブレンドの度合い(係数)のセット
			void	SetBlendPower(float fPower);
			//!	ブレンドの度合い(係数)の取得
			float	GetBlendPower(void);
			//!	現在のフレーム取得
			float	GetFrame(void);
			//!	モーション移行フラグの取得
			bool	GetMotionShiftFlag(void);
			//!	モーション数の取得
			word	GetMotionNum(void);
			//!	現在のモーション取得
			word	GetMotion(void);
			//!	ボーンの取得
			Matrix*	GetBone(word wNo);

		private:
			//!	スキンメッシュのフレーム更新
			void	UpdateSkinMeshFrame(word wNo, float fFrame, Quaternion* pOutPose, Vector3* pOutPos);
			//!	スキンメッシュのフレーム更新(モーションブレンド)
			void	UpdateSkinMeshFrame(word wNo, float fFrame1, float fFrame2, Quaternion* pOutPose, Vector3* pOutPos);
			//!	モーションの移行
			void	UpdateMotionShift(void);
			//!	モーションの更新
			void	UpdateMotion(void);
			//!	ボーンの更新
			void	UpdateBone(void);
			//!	スキンメッシュの更新
			void	UpdateSkinMesh(void);

		private:
			Vertex*			_pVertexs;		//!< 基本頂点
			LPD3DXSKININFO	_pSkinInfo;		//!< スキンメッシュ情報

			Matrix*			_pMatrix;		//!< マトリックス
			word			_wBoneNum;		//!< ボーン数
			Bone*			_pBone;			//!< ボーン情報
			word			_wMotionNum;	//!< モーション数
			Motion*			_pMotion;		//!< モーション情報
			word			_wMotion;		//!< モーション番号
			float			_fFrame;		//!< フレーム
			word*			_pDrawMotion;	//!< 描画しているモーション
			float*			_pDrawFrame;	//!< 描画しているフレーム
			bool			_bLoop;			//!< ループフラグ

			word			_wBlendMotion;	//!< ブレンドモーション番号
			float			_fBlendPower;	//!< ブレンドする度合い(係数)

			float			_fChangeFrame;	//!< 次のモーションに移行するまでのフレーム
		};
	}
}

//============================================================================
//	END OF FILE
//============================================================================