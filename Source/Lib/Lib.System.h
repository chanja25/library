//---------------------------------------------------------------------------
//!
//!	@file	Lib.System.h
//!	@brief	システム
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"
#include "Lib.System.Window.h"
#include "Lib.System.DirectX.h"

namespace Lib
{
	namespace System
	{
		//===========================================================================
		//!	システムクラス
		//===========================================================================
		class System
		{
		public:
			//!	コンストラクタ
			System(void);
			//!	デストラクタ
			~System(void);

			//!	システム初期化
			bool	InitSystem(const char* pWindowName, bool bFullScreen = false, bool bMultiple = false);
			//!	システム解放
			void	CleanupSystem(void);

			//!	メインループ
			void	MainLoop(void);

			//!	ウィンドウプロシージャーの設定
			void	SetWindowProc(WNDPROC WndProc);
			//!	ウィンドウサイズの設定
			void	SetWindowSize(int iWidth, int iHeight);

			//!	ウィンドウクラスの取得
			Window::Window*		GetWindow(void);
			//!	DirectXクラスの取得
			DirectX::DirectX*	GetDirectX(void);
			//!	Direct3Dデバイス
			LPDIRECT3DDEVICE9	GetDevice(void);

			//!	終了コードの取得
			int		GetExitCode(void);
			//!	インスタンスの取得
			static System*	GetInstance(void);

		protected:
			//!	初期化
			virtual	bool	Init(void) = 0;
			//!	解放
			virtual	void	Cleanup(void) = 0;

			//!	更新
			virtual	void	Update(double dElapsedTime) = 0;
			//!	描画
			virtual void	Render(double dElapsedTime) = 0;

		protected:
			Window::Window*			_pWindow;	//!< ウィンドウクラス
			DirectX::DirectX*		_pDirectX;	//!< DirectXクラス
			Frame::FrameControl*	_pFrame;	//!< フレームコントロール

		private:
			static System*			_pInstance;	//!< インスタンス
			LPDIRECT3DDEVICE9		_pDevice;	//!< Direct3Dデバイス
			int						_iWidth;	//!< ウィンドウの横幅
			int						_iHeight;	//!< ウィンドウの縦幅
			WNDPROC					_WndProc;	//!< ウィンドウプロシージャー
			int						_iExitCode;	//!< 終了コード
		};

		//!	ウインドウプロシージャー
		LRESULT	CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	}
}

//============================================================================
//	END OF FILE
//============================================================================