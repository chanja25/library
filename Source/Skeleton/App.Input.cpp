//---------------------------------------------------------------------------
//!
//!	@file	App.Input.cpp
//!	@brief	インプット
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
// --------------------------------------------------------------------------
//	include
// --------------------------------------------------------------------------
#include "App.h"

namespace App
{
	namespace Input
	{
		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Input::Input(void)
			: _pKeyboard(null)
		{
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		Input::~Input(void)
		{
			Lib::SafeDelete(_pKeyboard);
		}

		//---------------------------------------------------------------------------
		//	初期化
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Input::Init(void)
		{
			//	キーボードクラスの生成
			_pKeyboard = new Lib::InputManager::Keyboard;

			//	キーの初期設定
			SetKeyMapping(EKeyCode::CROSS, DIK_Z);
			SetKeyMapping(EKeyCode::CIRCLE, DIK_X);
			SetKeyMapping(EKeyCode::SQUARE, DIK_C);
			SetKeyMapping(EKeyCode::TRIANGLE, DIK_V);
			SetKeyMapping(EKeyCode::L1, DIK_A);
			SetKeyMapping(EKeyCode::R1, DIK_S);
			SetKeyMapping(EKeyCode::L2, DIK_Q);
			SetKeyMapping(EKeyCode::R2, DIK_W);
			SetKeyMapping(EKeyCode::START, DIK_RETURN);
			SetKeyMapping(EKeyCode::SELECT, DIK_SPACE);
			SetKeyMapping(EKeyCode::UP, DIK_UP);
			SetKeyMapping(EKeyCode::DOWN, DIK_DOWN);
			SetKeyMapping(EKeyCode::LEFT, DIK_LEFT);
			SetKeyMapping(EKeyCode::RIGHT, DIK_RIGHT);

			return true;
		}									  

		//---------------------------------------------------------------------------
		//	キーのセット
		//!	@param iKey [in] キー番号
		//!	@param iKeyCode [in] キーコード
		//---------------------------------------------------------------------------
		void	Input::SetKeyMapping(int iKey, int iKeyCode)
		{
			_KeyMap[iKey] = iKeyCode;
		}

		//---------------------------------------------------------------------------
		//	キーが押されているかチェック
		//!	@param iKey [in] キー
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Input::IsKeyPress(int iKey)
		{
			if( _KeyMap.find(iKey) != _KeyMap.end() )
				return _pKeyboard->IsKeyPress(_KeyMap[iKey]);

			return _pKeyboard->IsKeyPress(iKey);
		}

		//---------------------------------------------------------------------------
		//	キーを押したかチェック
		//!	@param iKey [in] キー
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Input::IsKeyPush(int iKey)
		{
			if( _KeyMap.find(iKey) != _KeyMap.end() )
				return _pKeyboard->IsKeyPush(_KeyMap[iKey]);

			return _pKeyboard->IsKeyPush(iKey);
		}

		//---------------------------------------------------------------------------
		//	キーが離されたかチェック
		//!	@param iKey [in] キー
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Input::IsKeyRelease(int iKey)
		{
			if( _KeyMap.find(iKey) != _KeyMap.end() )
				return _pKeyboard->IsKeyRelease(_KeyMap[iKey]);

			return _pKeyboard->IsKeyRelease(iKey);
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================