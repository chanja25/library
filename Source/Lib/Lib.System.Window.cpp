//---------------------------------------------------------------------------
//!
//!	@file	Lib.System.Window.cpp
//!	@brief	ウィンドウ関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//	include
//---------------------------------------------------------------------------
#include "Lib.h"
#include "Shellapi.h"

namespace Lib
{
	namespace System
	{
		namespace Window
		{
			//---------------------------------------------------------------------------
			//	コンストラクタ
			//---------------------------------------------------------------------------
			Window::Window(void)
				: _hWnd(null), _iWidth(1280), _iHeight(720)
				, _bFullScreen(false), _WndProc(null)
			{
			}

			//---------------------------------------------------------------------------
			//	コンストラクタ
			//!	@param iWidth [in] 横サイズ
			//!	@param iHeight [in] 縦サイズ
			//---------------------------------------------------------------------------
			Window::Window(int iWidth, int iHeight)
				: _hWnd(null), _iWidth(iWidth), _iHeight(iHeight)
				, _bFullScreen(false), _WndProc(null)
			{
			}

			//---------------------------------------------------------------------------
			//	デストラクタ
			//---------------------------------------------------------------------------
			Window::~Window(void)
			{
				Cleanup();
			}

			//---------------------------------------------------------------------------
			//	初期化
			//!	@param pName [in] ウィンドウ名
			//!	@param bFullScreen [in] フルスクリーンフラグ
			//!	@param bMultiple [in] 多重起動の可否
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	Window::Init(const char* pWindowName, bool bFullScreen, bool bMultiple)
			{
				char* pClassName = "Lib System";

				//	多重起動制限
				if( FindWindow(pClassName, pWindowName) && !bMultiple )
				{
					Message("既に起動しています！！", "警告");
					return false;
				}

				_bFullScreen = bFullScreen;

				if( _WndProc == null )
					_WndProc = WindowProc;

				//---------------------------------------------------------------------------
				//	ウインドウクラスの設定
				//---------------------------------------------------------------------------
				WNDCLASSEX wcex;

				wcex.cbSize			= sizeof(WNDCLASSEX);
				wcex.style			= CS_HREDRAW | CS_VREDRAW;
				wcex.lpfnWndProc	= _WndProc;
				wcex.cbClsExtra		= 0;
				wcex.cbWndExtra		= 0;
				wcex.hInstance		= GetModuleHandle(null);
				wcex.hIcon			= LoadIcon(wcex.hInstance, "Icon");
				wcex.hCursor		= LoadCursor(null, IDC_ARROW);
				wcex.hbrBackground	= (HBRUSH)GetStockObject(BLACK_BRUSH);
				wcex.lpszMenuName	= null;
				wcex.lpszClassName	= pClassName;
				wcex.hIconSm		= null;

				RegisterClassEx(&wcex);

				RECT Rect;
				Rect.left	= 0;
				Rect.top	= 0;
				Rect.right	= _iWidth;
				Rect.bottom	= _iHeight;

				dword dwStyle = (_bFullScreen)? WS_POPUP: WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
				AdjustWindowRect(&Rect, dwStyle, null);

				//---------------------------------------------------------------------------
				//	ウインドウの生成
				//---------------------------------------------------------------------------
				HWND hWnd;
				if( _bFullScreen == false )
				{
					//	ウインドウ
					hWnd = CreateWindow(wcex.lpszClassName,
										pWindowName,
										dwStyle,
										0,
										0,
										Rect.right - Rect.left,
										Rect.bottom - Rect.top,
										null,
										null,
										GetModuleHandle(null),
										null);
				}
				else
				{
					//	フルスクリーン
					hWnd = CreateWindow(wcex.lpszClassName,
										pWindowName,
										dwStyle,
										0,
										0,
										Rect.right - Rect.left,
										Rect.bottom - Rect.top,
										null,
										null,
										GetModuleHandle(null),
										null);
					ShowCursor(false);
				}

				//	ウインドウの表示
				ShowWindow(hWnd, SW_SHOW);
				UpdateWindow(hWnd);

				_hWnd = hWnd;

				return true;
			}

			//---------------------------------------------------------------------------
			//	解放
			//---------------------------------------------------------------------------
			void	Window::Cleanup(void)
			{
			}

			//---------------------------------------------------------------------------
			//	ウィンドウハンドルの取得
			//!	@return ウィンドウハンドル
			//---------------------------------------------------------------------------
			HWND	Window::GetWindowHandle(void)
			{
				return _hWnd;
			}

			//---------------------------------------------------------------------------
			//	画面横サイズの取得
			//!	@return 横サイズ
			//---------------------------------------------------------------------------
			int		Window::GetWidth(void)
			{
				return _iWidth;
			}

			//---------------------------------------------------------------------------
			//	画面縦サイズの取得
			//!	@return 縦サイズ
			//---------------------------------------------------------------------------
			int		Window::GetHeight(void)
			{
				return _iHeight;
			}

			//---------------------------------------------------------------------------
			//	フルスクリーンフラグの取得
			//!	@return フルスクリーンフラグ
			//---------------------------------------------------------------------------
			bool	Window::GetFullScreen(void)
			{
				return _bFullScreen;
			}

			//---------------------------------------------------------------------------
			//	ウィンドウプロシージャーの設定
			//!	@param WndProc [in] ウィンドウプロシージャー
			//---------------------------------------------------------------------------
			void	Window::SetWindowProc(WNDPROC WndProc)
			{
				_WndProc = WndProc;
			}
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================