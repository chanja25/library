//---------------------------------------------------------------------------
//!
//!	@file	App.Scene.h
//!	@brief	シーン
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

// --------------------------------------------------------------------------
//	include
// --------------------------------------------------------------------------
#include "App.h"

namespace App
{
	namespace Scene
	{
		//===========================================================================
		//!	テストシーン
		//===========================================================================
		class SceneTest
			: public Lib::SceneManager::SceneBase
		{
		public:
			//	コンストラクタ
			SceneTest(void);
			//	デストラクタ
			~SceneTest(void);

			//!	初期化
			bool	Init(void);
			//!	解放
			void	Cleanup(void);
			//!	初期化(ロード用)
			bool	InitLoad(void);
			//!	解放(ロード用)
			void	CleanupLoad(void);

			//!	更新
			void	Update(double dElapsedTime);
			//!	描画
			void	Render(double dElapsedTime);
			//!	更新(ロード用)
			void	UpdateLoad(double dElapsedTime);
			//!	描画(ロード用)
			void	RenderLoad(double dElapsedTime);

		private:
		};

		//===========================================================================
		//!	タイトルシーン
		//===========================================================================
		class SceneTitle
			: public Lib::SceneManager::SceneBase
		{
		public:
			//	コンストラクタ
			SceneTitle(void);
			//	デストラクタ
			~SceneTitle(void);

			//!	初期化
			bool	Init(void);
			//!	解放
			void	Cleanup(void);

			//!	更新
			void	Update(double dElapsedTime);
			//!	描画
			void	Render(double dElapsedTime);

		private:
		};

		//===========================================================================
		//!	ゲームシーン
		//===========================================================================
		class SceneGame
			: public Lib::SceneManager::SceneBase
		{
		public:
			//	コンストラクタ
			SceneGame(void);
			//	デストラクタ
			~SceneGame(void);

			//!	初期化
			bool	Init(void);
			//!	解放
			void	Cleanup(void);
			//!	初期化(ロード用)
			bool	InitLoad(void);
			//!	解放(ロード用)
			void	CleanupLoad(void);

			//!	更新
			void	Update(double dElapsedTime);
			//!	描画
			void	Render(double dElapsedTime);
			//!	更新(ロード用)
			void	UpdateLoad(double dElapsedTime);
			//!	描画(ロード用)
			void	RenderLoad(double dElapsedTime);

		private:
			Lib::Shader::Shader*	_pShader;	//!< シェーダー
			Lib::Camera::Camera*	_pCamera;	//!< カメラ
			App::Game::Player*		_pPlayer;	//!< プレイヤー
		};
	}
}

//============================================================================
//	END OF FILE
//============================================================================