//---------------------------------------------------------------------------
//!
//!	@file	Lib.SpriteManager.cpp
//!	@brief	スプライト管理
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace SpriteManager
	{
		//!	頂点情報
		struct SpriteVertex
		{
			Vector4 Position;
			dword	dwColor;
			Vector2 UV;
		};
		//!	頂点フォーマット
		const dword D3DFVF_SPRITEVERTEX = D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1;

		//!	描画用頂点
		SpriteVertex	Vertex[4];

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Sprite::Sprite(void)
			: _pTexture(null)
			, _DestRect(0.0f, 0.0f, 0.0f, 0.0f)
			, _SrcRect(0.0f, 0.0f, 0.0f, 0.0f)
			, _dwColor(0xFFFFFFFF)
		{
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//!	@param pFileName [in] ファイル名
		//---------------------------------------------------------------------------
		Sprite::Sprite(const char* pFileName)
			: _pTexture(null)
			, _DestRect(0.0f, 0.0f, 0.0f, 0.0f)
			, _SrcRect(0.0f, 0.0f, 0.0f, 0.0f)
			, _dwColor(0xFFFFFFFF)
		{
			Load(pFileName);
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		Sprite::~Sprite(void)
		{
			Lib::TextureManager::Delete(&_pTexture);
		}

		//---------------------------------------------------------------------------
		//	読み込み
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Sprite::Load(const char* pFileName)
		{
			//	テクスチャー解放
			Lib::TextureManager::Delete(&_pTexture);
			//	テクスチャーの作成
			if( !Lib::TextureManager::Create(&_pTexture, pFileName) )
				return false;

			LPDIRECT3DSURFACE9	pTemp;
			D3DSURFACE_DESC		desc;
			//	テクスチャー情報取得
			_pTexture->GetSurfaceLevel(0, &pTemp);
			pTemp->GetDesc(&desc);

			SafeRelease(pTemp);

			_fWidth  = (float)desc.Width;
			_fHeight = (float)desc.Height;

			return true;
		}

		//---------------------------------------------------------------------------
		//	テクスチャー生成
		//!	@param fWidth [in] 横幅
		//!	@param fHeight [in] 縦幅
		//!	@param iFormat [in] フォーマット
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Sprite::Create(dword dwWidth, dword dwHeight, int iFormat)
		{
			//	テクスチャー解放
			Lib::TextureManager::Delete(&_pTexture);
			//	テクスチャーの作成
			if( !Lib::TextureManager::Create(&_pTexture, dwWidth, dwHeight, iFormat) )
				return false;

			_fWidth  = (float)dwWidth;
			_fHeight = (float)dwHeight;
			return true;
		}

		//---------------------------------------------------------------------------
		//	描画
		//!	@param pShader [in] シェーダー
		//!	@param pTechnique [in] テクニック
		//!	@param dwPass [in] パス
		//---------------------------------------------------------------------------
		void	Sprite::Render(Shader::Shader* pShader, char* pTechnique, dword dwPass)
		{
			pShader->SetTechnique(pTechnique);
			pShader->BeginShader();

			pShader->SetTexture("DiffuseMap", _pTexture);
			pShader->BeginPass(dwPass);
			pShader->CommitChanges();

			Render();

			pShader->EndPass();
			pShader->EndShader();
		}

		//---------------------------------------------------------------------------
		//	描画
		//---------------------------------------------------------------------------
		void	Sprite::Render(void)
		{
			//	表示位置セット
			Vertex[0].Position.x = Vertex[2].Position.x = _DestRect.x;
			Vertex[1].Position.x = Vertex[3].Position.x = _DestRect.x + _DestRect.z;

			Vertex[0].Position.y = Vertex[1].Position.y = _DestRect.y;
			Vertex[2].Position.y = Vertex[3].Position.y = _DestRect.y + _DestRect.w;

			//	テクスチャーの座標セット
			Vertex[0].UV.x = Vertex[2].UV.x = (_SrcRect.x + 0.5f) / _fWidth;
			Vertex[1].UV.x = Vertex[3].UV.x = (_SrcRect.x + _SrcRect.z) / _fWidth;

			Vertex[0].UV.y = Vertex[1].UV.y = (_SrcRect.y + 0.5f) / _fHeight;
			Vertex[2].UV.y = Vertex[3].UV.y = (_SrcRect.y + _SrcRect.w) / _fHeight;

			Vertex[0].Position.z = Vertex[1].Position.z = Vertex[2].Position.z = Vertex[3].Position.z = 0.0f;
			Vertex[0].Position.w = Vertex[1].Position.w = Vertex[2].Position.w = Vertex[3].Position.w = 1.0f;
			Vertex[0].dwColor = Vertex[1].dwColor = Vertex[2].dwColor = Vertex[3].dwColor = _dwColor;

			LPDIRECT3DDEVICE9 pDevice = Lib::System::System::GetInstance()->GetDevice();
			pDevice->SetFVF(D3DFVF_SPRITEVERTEX);
			pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, &Vertex, sizeof(SpriteVertex));
		}

		//---------------------------------------------------------------------------
		//	位置、サイズのセット
		//!	@param fX [in] X座標
		//!	@param fY [in] Y座標
		//!	@param fWidth [in] 横幅
		//!	@param fHeight [in] 縦幅
		//---------------------------------------------------------------------------
		void	Sprite::SetRect(float fX, float fY, float fWidth, float fHeight)
		{
			_DestRect.x = fX;
			_DestRect.y = fY;
			_DestRect.z = fWidth;
			_DestRect.w = fHeight;
		}

		//---------------------------------------------------------------------------
		//	位置のセット
		//!	@param fX [in] X座標
		//!	@param fY [in] Y座標
		//---------------------------------------------------------------------------
		void	Sprite::SetPosition(float fX, float fY)
		{
			_DestRect.x = fX;
			_DestRect.y = fY;
		}

		//---------------------------------------------------------------------------
		//	サイズのセット
		//!	@param fWidth [in] 横幅
		//!	@param fHeight [in] 縦幅
		//---------------------------------------------------------------------------
		void	Sprite::SetSize(float fWidth, float fHeight)
		{
			_DestRect.z = fWidth;
			_DestRect.w = fHeight;
		}

		//---------------------------------------------------------------------------
		//	位置、サイズの取得
		//!	@return 位置、サイズ
		//---------------------------------------------------------------------------
		Vector4	Sprite::GetRect(void)
		{
			return _DestRect;
		}

		//---------------------------------------------------------------------------
		//	位置の取得
		//!	@return 位置
		//---------------------------------------------------------------------------
		Vector2	Sprite::GetPosition(void)
		{
			return Vector2(_DestRect.x, _DestRect.y);
		}

		//---------------------------------------------------------------------------
		//	サイズの取得
		//!	@return サイズ
		//---------------------------------------------------------------------------
		Vector2	Sprite::GetSize(void)
		{
			return Vector2(_DestRect.z, _DestRect.w);
		}

		//---------------------------------------------------------------------------
		//	画像内の位置、サイズのセット
		//!	@param fX [in] 画像内のX座標
		//!	@param fY [in] 画像内のY座標
		//!	@param fWidth [in] 画像内の横幅
		//!	@param fHeight [in] 画像内の縦幅
		//---------------------------------------------------------------------------
		void	Sprite::SetSrcRect(float fX, float fY, float fWidth, float fHeight)
		{
			_SrcRect.x = fX;
			_SrcRect.y = fY;
			_SrcRect.z = fWidth;
			_SrcRect.w = fHeight;
		}

		//---------------------------------------------------------------------------
		//	画像内の位置のセット
		//!	@param fX [in] 画像内のX座標
		//!	@param fY [in] 画像内のY座標
		//---------------------------------------------------------------------------
		void	Sprite::SetSrcPosition(float fX, float fY)
		{
			_SrcRect.x = fX;
			_SrcRect.y = fY;
		}

		//---------------------------------------------------------------------------
		//	画像内のサイズのセット
		//!	@param fWidth [in] 画像内の横幅
		//!	@param fHeight [in] 画像内の縦幅
		//---------------------------------------------------------------------------
		void	Sprite::SetSrcSize(float fWidth, float fHeight)
		{
			_SrcRect.z = fWidth;
			_SrcRect.w = fHeight;
		}

		//---------------------------------------------------------------------------
		//	画像内の位置、サイズの取得
		//!	@return 画像内の位置、サイズ
		//---------------------------------------------------------------------------
		Vector4	Sprite::GetSrcRect(void)
		{
			return _SrcRect;
		}

		//---------------------------------------------------------------------------
		//	画像内の位置の取得
		//!	@return 画像内の位置
		//---------------------------------------------------------------------------
		Vector2	Sprite::GetSrcPosition(void)
		{
			return Vector2(_SrcRect.x, _SrcRect.y);
		}

		//---------------------------------------------------------------------------
		//	画像内のサイズの取得
		//!	@return 画像内のサイズ
		//---------------------------------------------------------------------------
		Vector2	Sprite::GetSrcSize(void)
		{
			return Vector2(_SrcRect.z, _SrcRect.w);
		}

		//---------------------------------------------------------------------------
		//	色のセット
		//!	@param dwColor [in] 色
		//---------------------------------------------------------------------------
		void	Sprite::SetColor(dword dwColor)
		{
			_dwColor = dwColor;
		}

		//---------------------------------------------------------------------------
		//	テクスチャーの取得
		//!	@return テクスチャー
		//---------------------------------------------------------------------------
		LPDIRECT3DTEXTURE9	Sprite::GetTexture(void)
		{
			return _pTexture;
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================