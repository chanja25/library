//---------------------------------------------------------------------------
//!
//!	@file	App.Scene.SceneGame.cpp
//!	@brief	ゲームシーン
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "App.h"

namespace App
{
	namespace Scene
	{
		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		SceneGame::SceneGame(void)
			: _pPlayer(null)
			, _pShader(null)
			, _pCamera(null)
		{
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		SceneGame::~SceneGame(void)
		{
		}

		//---------------------------------------------------------------------------
		//	初期化
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	SceneGame::Init(void)
		{
			//	シェーダー生成
			_pShader = new Lib::Shader::Shader;
			_pShader->Init("Data\\Effect\\3D.fx");

			//	カメラ生成
			_pCamera = new Lib::Camera::Camera;
			_pCamera->Init();
			_pCamera->SetProjection(D3DXToRadian(45), 1.0f, 1000.0f);
			_pCamera->SetPos(Lib::Vector3(0, 3, -10));
			_pCamera->SetTarget(Lib::Vector3(0, 1, 0));

			//	プレイヤー生成
			_pPlayer = new App::Game::Player;
			_pPlayer->Init();

			return true;
		}

		//---------------------------------------------------------------------------
		//	解放
		//---------------------------------------------------------------------------
		void	SceneGame::Cleanup(void)
		{
			Lib::SafeDelete(_pShader);
			Lib::SafeDelete(_pCamera);
			Lib::SafeDelete(_pPlayer);
		}

		//---------------------------------------------------------------------------
		//	初期化(ロード用)
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	SceneGame::InitLoad(void)
		{
			return true;
		}

		//---------------------------------------------------------------------------
		//	解放(ロード用)
		//---------------------------------------------------------------------------
		void	SceneGame::CleanupLoad(void)
		{
		}

		//---------------------------------------------------------------------------
		//	更新
		//!	@param dElapsedTime [in] 経過時間
		//---------------------------------------------------------------------------
		void	SceneGame::Update(double dElapsedTime)
		{
			//	カメラ更新
			_pCamera->Update();

			//	シェーダーに情報セット
			_pShader->SetVector3("vEyePos", _pCamera->GetPos());
			_pShader->SetMatrix("mViewProjection", _pCamera->GetView() * _pCamera->GetProjection());

			//	プレイヤー更新
			_pPlayer->Update();

			//	シーン移行
			if( pInput->IsKeyPush(EKeyCode::START) )
				pSceneManager->JumpScene(ESceneMode::TITLE);
		}

		//---------------------------------------------------------------------------
		//	描画
		//!	@param dElapsedTime [in] 経過時間
		//---------------------------------------------------------------------------
		void	SceneGame::Render(double dElapsedTime)
		{
			//	プレイヤー描画
			_pPlayer->Render(_pShader);

#if	_DEBUG
			pDebugFont->Draw(1280, 0, "Scene:Game", 0xff00ff00, Lib::EFormat::RIGHT);
#endif	// ~#if _DEBUG
		}

		//---------------------------------------------------------------------------
		//	更新(ロード用)
		//!	@param dElapsedTime [in] 経過時間
		//---------------------------------------------------------------------------
		void	SceneGame::UpdateLoad(double dElapsedTime)
		{
		}

		//---------------------------------------------------------------------------
		//	描画(ロード用)
		//!	@param dElapsedTime [in] 経過時間
		//---------------------------------------------------------------------------
		void	SceneGame::RenderLoad(double dElapsedTime)
		{
#if	_DEBUG
			pDebugFont->Draw(1280, 0, "Scene:Game(Load)", 0xff00ff00, Lib::EFormat::RIGHT);
#endif	// ~#if _DEBUG
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================