//---------------------------------------------------------------------------
//!
//!	@file	Lib.Shader.h
//!	@brief	シェーダー
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace Shader
	{
		//===========================================================================
		//!	シェーダークラス
		//===========================================================================
		class Shader
		{
		public:
			//!	コンストラクタ
			Shader(void);
			//!	デストラクタ
			~Shader(void);

			//!	初期化
			bool	Init(const char* pFileName);
			//!	解放
			void	Cleanup(void);

			//!	コミットチェンジ
			void	CommitChanges(void);
			//!	シェーダーの開始
			void	BeginShader(void);
			//!	シェーダーの終了
			void	EndShader(void);
			//!	パスの開始
			void	BeginPass(unsigned int uPass);
			//!	パスの終了
			void	EndPass(void);
			//!	パスの数の取得
			unsigned int	GetPass(void);

			//!	テクニックのセット
			void	SetTechnique(char* pTechnique);
			//!	Textureのセット
			void	SetTexture(char* pName, const LPDIRECT3DTEXTURE9 pTexture);
			//!	Matrixのセット
			void	SetMatrix(char* pName, const Matrix& Param);
			//!	intのセット
			void	SetInt(char* pName, int Param);
			//!	intのセット
			void	SetIntArray(char* pName, const int* pParam, dword dwNum);
			//!	floatのセット
			void	SetFloat(char* pName, float Param);
			//!	floatのセット
			void	SetFloatArray(char* pName, const float* pParam, dword dwNum);
			//!	Vecot3のセット
			void	SetVector3(char* pName, const Vector3& Param);
			//!	floatのセット
			void	SetVector3Array(char* pName, const Vector3* pParam, dword dwNum);
			//!	Vecot3のセット
			void	SetVector4(char* pName, const Vector4& Param);
			//!	floatのセット
			void	SetVector4Array(char* pName, const Vector4* pParam, dword dwNum);

		private:
			LPD3DXEFFECT	_pEffect;	//!< エフェクト
			D3DXHANDLE		_Technique;	//!< テクニック

			unsigned int	_uPass;		//!< パス数
		};
	}
}

//============================================================================
//	END OF FILE
//============================================================================