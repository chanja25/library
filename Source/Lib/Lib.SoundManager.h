//---------------------------------------------------------------------------
//!
//!	@file	Lib.SoundManager.h
//!	@brief	サウンド関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace SoundManager
	{
		//===========================================================================
		//!	ストリームベースクラス
		//===========================================================================
		class StreamBase
		{
		public:
			//!	コンストラクタ
			StreamBase(void) {}
			//!	デストラクタ
			virtual ~StreamBase(void) {}

			//!	再生
			virtual void	Play(void) = 0;
			//!	停止
			virtual void	Stop(void) = 0;
			//!	再生フラグ取得
			virtual bool	IsPlay(void) = 0;

			//!	音量変更
			virtual void	SetVolume(int iVolume) = 0;
			//!	パン変更
			virtual void	SetPan(int iPan) = 0;
			//! モードセット
			virtual void	SetMode(int iMode) = 0;
			//!	パラメータセット
			virtual void	SetParam(int iParam) = 0;
		};

		//!	ストリーム再生クラス生成
		bool	Create(char* pFileName, StreamBase** ppStream, int iMode = EStreamMode::NORMAL, int iParam = 1);
		//!	ストリーム再生クラス解放
		void	Delete(StreamBase** ppStream);

		//!	読み込み
		bool	Load(dword dwNo, char* pFileName);
		//!	解放
		void	Cleanup(dword dwNo);
		//!	解放
		void	CleanupAll(void);
		//!	再生
		void	Play(dword dwNo, bool bLoop);
		//!	停止
		void	Stop(dword dwNo);
		//!	停止
		void	Stop(void);
		//!	再生フラグ取得
		bool	IsPlay(dword dwNo);
		//!	音量変更
		void	SetVolume(dword dwNo, int iVolume);
		//!	パン変更
		void	SetPan(dword dwNo, int iPan);
		//!	再生速度変更
		void	SetSpeed(dword dwNo, float fSpeed);
	}
}

//============================================================================
//	END OF FILE
//============================================================================