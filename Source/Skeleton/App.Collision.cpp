//===========================================================================
//!
//!	@file		App.Collision.cpp
//!	@brief		当たり判定関連
//!
//!	@author S.Kawamoto
//===========================================================================
#include "App.h"

namespace App
{
	namespace Collision
	{
		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Object::Object(void)
			: _Pos(0.0f, 0.0f, 0.0f), _Pos2(0.0f, 0.0f, 0.0f)
			, _HitPos(0.0f, 0.0f, 0.0f), _fRadius(0.0f)
			, _iState(0), _iShape(0)
			, _iAttribute(0), _iHitAttribute(0)
			, _bHitDelete(false)
		{
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		Object::~Object(void)
		{
		}

		//---------------------------------------------------------------------------
		//	初期化
		//!	@param iSshape [in] 形
		//!	@param iAttribute [in] 自分の属性
		//!	@param iHitAttribute [in] 当てる事ができる属性
		//!	@param iHitDelete [in] 衝突時に当たり判定を消すか否か
		//---------------------------------------------------------------------------
		void	Object::Init(int iShape, int iAttribute, int iHitAttribute, bool bHitDelete)
		{
			_iShape = iShape;
			_iAttribute = iAttribute;
			_iHitAttribute = iHitAttribute;
			_bHitDelete = bHitDelete;
		}

		//---------------------------------------------------------------------------
		//	オブジェクト当たりのセット
		//!	@param Pos [in] 位置
		//!	@param fRadius [in] 半径
		//!	@param Pos2 [in] 位置2(カプセルの場合使う)
		//---------------------------------------------------------------------------
		void	Object::SetObject(const Lib::Vector3& Pos, float fRadius, const Lib::Vector3& Pos2)
		{
			_iState = 0;
			_Pos = Pos;
			_fRadius = fRadius;
			_Pos2 = Pos2;
			pCollision->SetObject(this);
		}

		//---------------------------------------------------------------------------
		//	衝突した位置の設定
		//!	@param Pos [in] 位置
		//---------------------------------------------------------------------------
		void	Object::SetHitPos(const Lib::Vector3& Pos)
		{
			_HitPos = Pos;
		}

		//---------------------------------------------------------------------------
		//	ステータスのセット
		//!	@param Pos [in] 位置
		//---------------------------------------------------------------------------
		void	Object::SetState(int iState)
		{
			_iState = iState;
		}

		//---------------------------------------------------------------------------
		//	位置の取得
		//!	@return 位置
		//---------------------------------------------------------------------------
		Lib::Vector3	Object::GetPos(void)
		{
			return _Pos;
		}

		//---------------------------------------------------------------------------
		//	カプセル用の位置の取得
		//!	@return 位置
		//---------------------------------------------------------------------------
		Lib::Vector3	Object::GetPos2(void)
		{
			return _Pos2;
		}

		//---------------------------------------------------------------------------
		//	衝突位置の取得
		//!	@return 衝突位置
		//---------------------------------------------------------------------------
		Lib::Vector3	Object::GetHitPos(void)
		{
			return _HitPos;
		}

		//---------------------------------------------------------------------------
		//	半径の取得
		//!	@return 半径
		//---------------------------------------------------------------------------
		float	Object::GetRadius(void)
		{
			return _fRadius;
		}

		//---------------------------------------------------------------------------
		//	状態の取得
		//!	@return 状態
		//---------------------------------------------------------------------------
		int		Object::GetState(void)
		{
			return _iState;
		}

		//---------------------------------------------------------------------------
		//	形の取得
		//!	@return 形
		//---------------------------------------------------------------------------
		int		Object::GetShape(void)
		{
			return _iState;
		}

		//---------------------------------------------------------------------------
		//	属性の取得
		//!	@return 属性
		//---------------------------------------------------------------------------
		int		Object::GetAttribute(void)
		{
			return _iAttribute;
		}

		//---------------------------------------------------------------------------
		//	攻撃属性の取得
		//!	@return 攻撃属性
		//---------------------------------------------------------------------------
		int		Object::GetHitAttribute(void)
		{
			return _iHitAttribute;
		}

		//---------------------------------------------------------------------------
		//	消滅フラグの取得
		//!	@return 消滅フラグ
		//---------------------------------------------------------------------------
		bool	Object::GetHitDelete(void)
		{
			return _bHitDelete;
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Collision::Collision(void)
		{
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		Collision::~Collision(void)
		{
		}

		//---------------------------------------------------------------------------
		//	解放
		//---------------------------------------------------------------------------
		void	Collision::Cleanup(void)
		{
			_ObjectList.clear();
			_AttackList.clear();
		}

		//---------------------------------------------------------------------------
		//	更新
		//---------------------------------------------------------------------------
		void	Collision::Update(void)
		{
			std::vector<Object*>::iterator itObject;
			std::vector<Object*>::iterator itAttack;
			//	当たり判定の先頭を取得
			itObject = _ObjectList.begin();

			while( itObject != _ObjectList.end() )
			{
				//	攻撃判定の先頭を取得
				itAttack = _AttackList.begin();
				while( itAttack != _AttackList.end() )
				{
					//	オブジェクト同士の当たりを判定する
					if( CollisionObject(*itObject, *itAttack) )
					{
						//	当たっていればステータスをセットしてループを進める
						(*itObject)->SetState((*itAttack)->GetAttribute());
						(*itAttack)->SetState((*itObject)->GetAttribute());

						//	貫通属性がなければリストから当たり判定を削除
						if( (*itAttack)->GetHitDelete() == false )
							_AttackList.erase(itAttack);
						break;
					}

					itAttack++;
				}
				//	リストからオブジェクトを削除
				itObject = _ObjectList.erase(itObject);
			}

			//	リストから全ての当たり判定削除
			_AttackList.clear();
		}

		//---------------------------------------------------------------------------
		//	球と球の当たり判定
		//!	@param Pos1 [in] 1つ目の球の位置
		//!	@param fRadius1 [in] 1つ目の球の半径
		//!	@param Pos2 [in] 2つ目の球の位置
		//!	@param fRadius2 [in] 2つ目の球の半径
		//!	@param pOut[out] 衝突した位置
		//!	@return	結果
		//---------------------------------------------------------------------------
		bool	Collision::CollisionSphere(const Lib::Vector3& Pos1, float fRadius1, const Lib::Vector3& Pos2, float fRadius2, Lib::Vector3* pOut)
		{
			//	2つの球の距離の2乗を求める
			float fLength = Lib::Math::Length2(Pos1 - Pos2);
			//	2つの球の大きさを足す
			float fRadius = fRadius1 + fRadius2;
			//	距離の2乗と2つの大きさの2乗を比較する
			if( fLength < (fRadius * fRadius) )
			{
				if( pOut )
				{
					//	衝突位置の算出
					Lib::Math::Normalize(pOut, Pos2 - Pos1);
					*pOut = Pos1 + *pOut * fRadius1;
				}
				return true;
			}
			return false;
		}

		//---------------------------------------------------------------------------
		//	球とカプセルの当たり判定
		//!	@param Pos [in] 球の位置
		//!	@param fRadius1 [in] 球の半径
		//!	@param Pos1 [in] カプセルの1つ目の半球の位置
		//!	@param Pos2 [in] カプセルの2つ目の半球の位置
		//!	@param fRadius2 [in] 2つ目の球の半径
		//!	@param pOut [out] 衝突した位置
		//!	@return	結果
		//---------------------------------------------------------------------------
		bool	Collision::CollisionCapsule(const Lib::Vector3& Pos, float fRadius1, const Lib::Vector3& Pos1, const Lib::Vector3& Pos2, float fRadius2, Lib::Vector3* pOut)
		{
			//	カプセルの1つ目の半球との当たり判定を取る
			if( CollisionSphere(Pos, fRadius1, Pos1, fRadius2, pOut) )
				return true;
			//	カプセルの2つ目の半球との当たり判定を取る
			if( CollisionSphere(Pos, fRadius1, Pos2, fRadius2, pOut) )
				return true;

			//	2つ目の半球と球の内積を取りcosθを算出
			float fCos = Lib::Math::Dot(Pos - Pos2, Pos1 - Pos2);
			//	cosθがマイナスならば鈍角なので当たっていない
			if( fCos < 0 )
				return false;

			//	1つ目の半球と球の内積を取りcosθを算出
			fCos = Lib::Math::Dot(Pos - Pos1, Pos2 - Pos1) / (Lib::Math::Length(Pos - Pos1) * Lib::Math::Length(Pos2 - Pos1));
			//	cosθがマイナスならば鈍角なので当たっていない
			if( fCos < 0 )
				return false;

			//	コサインθのからサインθの値を求める
			float fSin = sqrtf(1 - (fCos * fCos));
			float fLength = Lib::Math::Length(Pos - Pos1);

			//	最短距離の位置で当たっているか確認する
			if( (fSin * fLength) < (fRadius1 + fRadius2) )
			{
				if( pOut )
				{
					//	衝突位置の算出
					Lib::Math::Normalize(pOut, Pos1 - Pos);
					*pOut = Pos + *pOut * fRadius1;
				}
				return true;
			}
			return false;
		}

		//---------------------------------------------------------------------------
		//	オブジェクトの当たり判定
		//!	@param pObject [in] 当たりオブジェクト
		//!	@param pAttack [in] 攻撃オブジェクト
		//!	@return	結果
		//---------------------------------------------------------------------------
		bool	Collision::CollisionObject(Object* pObject, Object* pAttack)
		{
			Lib::Vector3 HitPos(0, 0, 0);
			bool bResult = false;

			//	攻撃の属性が当たり判定を行う属性か確認
			if( pObject->GetAttribute() & pAttack->GetHitAttribute() )
			{
				//	球
				if( pObject->GetShape() == ECollision::EShape::SPHERE )
				{
					//	球:球
					if( pAttack->GetShape() == ECollision::EShape::SPHERE )
						bResult = CollisionSphere(pObject->GetPos(), pObject->GetRadius(), pAttack->GetPos(), pAttack->GetRadius(), &HitPos);
					//	球:カプセル
					if( pAttack->GetShape() == ECollision::EShape::CAPSULE )
						bResult = CollisionCapsule(pObject->GetPos(), pObject->GetRadius(), pAttack->GetPos(), pAttack->GetPos2(), pAttack->GetRadius(), &HitPos);
				}

				//	カプセル
				if( pObject->GetShape() == ECollision::EShape::CAPSULE )
				{
					//	カプセル:球
					if( pAttack->GetShape() == ECollision::EShape::SPHERE )
						bResult = CollisionCapsule(pObject->GetPos(), pObject->GetRadius(), pAttack->GetPos(), pAttack->GetPos2(), pAttack->GetRadius(), &HitPos);
				}
			}

			//	衝突位置セット
			pObject->SetHitPos(HitPos);
			pAttack->SetHitPos(HitPos);

			return bResult;
		}

		//---------------------------------------------------------------------------
		//	当たり判定用オブジェクトのセット
		//!	@param pObject [in] セットするオブジェクト
		//---------------------------------------------------------------------------
		void	Collision::SetObject(Object* pObject)
		{
			//	攻撃属性が存在するか確認する
			if( pObject->GetHitAttribute() == ECollision::EAttribute::NONE )
				//	当たりオブジェクトに設定
				_ObjectList.push_back(pObject);
			else
				//	攻撃オブジェクトにセット
				_AttackList.push_back(pObject);
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================