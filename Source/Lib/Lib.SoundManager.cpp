//---------------------------------------------------------------------------
//!
//!	@file	Lib.SoundManager.cpp
//!	@brief	サウンド関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"
#include "ogg/vorbisfile.h"

//---------------------------------------------------------------------------
// library
//---------------------------------------------------------------------------
#pragma comment(lib, "../Lib/ogg/vorbis_static.lib")
#pragma comment(lib, "../Lib/ogg/ogg_static.lib")
#pragma comment(lib, "../Lib/ogg/vorbisfile_static.lib")

namespace Lib
{
	namespace SoundManager
	{
		//!	Wave
		const byte TYPE_WAVE = 0;
		//!	Ogg
		const byte TYPE_OGG = 1;

		//! サウンドオブジェクト
		LPDIRECTSOUND pDSound = null;
		//! 一次バッファ
		LPDIRECTSOUNDBUFFER pPrimary = null;

		//!	ストリーム管理
		unsigned int __stdcall	StreamFunction(void* pArg);

		//---------------------------------------------------------------------------
		//!	Vorbisfileコールバック(ファイル読み込み)
		//!	@param pPtr [out] 読み込んだファイルを入れるポインタ
		//!	@param dwSize [in] データのサイズ
		//!	@param dwNmemb [in] データの数
		//!	@param pDataSource [in] ハンドル
		//!	@retva ファイルサイズ
		//---------------------------------------------------------------------------
		unsigned	CallbackRead(void* pPtr, unsigned uSize, unsigned uNmemb, void* pDataSource)
		{
			HANDLE hFile = (HANDLE)pDataSource;
			dword dwWork = 0;
			ReadFile(hFile, pPtr, uSize * uNmemb, &dwWork, null);
			return dwWork;
		}

		//---------------------------------------------------------------------------
		//!	Vorbisfileコールバック(ファイルシーク)
		//!	@param pDataSource [in] ハンドル
		//!	@param iOffset [in] データの位置
		//!	@param iWhence) [in] シークするモード
		//!	@retva 結果
		//---------------------------------------------------------------------------
		int		CallbackSeek(void* pDataSource, __int64 iOffset, int iWhence)
		{
			HANDLE hFile = (HANDLE)pDataSource;
			dword dwResult;

			if( iWhence == SEEK_CUR )
				dwResult = SetFilePointer(hFile, (long)iOffset, null, FILE_CURRENT);
			else if( iWhence == SEEK_END )
				dwResult = SetFilePointer(hFile, (long)iOffset, null, FILE_END);
			else if( iWhence == SEEK_SET )
				dwResult = SetFilePointer(hFile, (long)iOffset, null, FILE_BEGIN);
			else
				return -1;

			return ( dwResult == -1 )? -1 : 0;
		}

		//---------------------------------------------------------------------------
		//!	Vorbisfileコールバック(ファイルクローズ)
		//!	@param pDataSource [in] ファイルのハンドル
		//!	@retval 結果
		//---------------------------------------------------------------------------
		int		CallbackClose(void* pDataSource)
		{
			HANDLE hFile = (HANDLE)pDataSource;
			int iResult = CloseHandle(hFile);
			return iResult ? 0 : EOF;
		}

		//---------------------------------------------------------------------------
		//!	Vorbisfileコールバック(ファイルの現在位置取得)
		//!	@param pDataSource [in] ファイルのハンドル
		//!	@retval 現在位置
		//---------------------------------------------------------------------------
		long	CallbackTell(void* pDataSource)
		{
			HANDLE hFile = (HANDLE)pDataSource;
			long lOffset = SetFilePointer(hFile, 0, null, FILE_CURRENT);
			return lOffset;
		}

		//===========================================================================
		//!	SoundBufferクラス
		//===========================================================================
		class SoundBuffer
		{
		public:
			//---------------------------------------------------------------------------
			//!	コンストラクタ
			//---------------------------------------------------------------------------
			SoundBuffer(void)
				: _pBuffer(null)
			{
			}

			//---------------------------------------------------------------------------
			//!	デストラクタ
			//---------------------------------------------------------------------------
			~SoundBuffer(void)
			{
				SafeRelease(_pBuffer);
			}

			//---------------------------------------------------------------------------
			//!	ファイルの読み込み
			//!	@param pFileName [in] ファイル名
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	Load(char* pFileName)
			{
				//	データがあれば解放しておく
				SafeRelease(_pBuffer);

				dword i;
				//	拡張子取得
				for(i = (dword)strlen(pFileName); i > 0; i--)
					if( pFileName[i - 1] == '.' )
						break;

				bool bResult = false;
				//	WAVEの読み込み
				if( strcmp(&pFileName[i], "wav") == 0 )
					bResult = LoadWave(pFileName);
				//	OGGの読み込み
				if( strcmp(&pFileName[i], "ogg") == 0 )
					bResult = LoadOgg(pFileName);

				//	初期設定
				SetVolume(255);
				SetPan(0);
				SetSpeed(1.0f);

				return bResult;
			}

			//---------------------------------------------------------------------------
			//!	再生
			//!	@param bLoop [in] ループフラグ
			//---------------------------------------------------------------------------
			void	Play(bool bLoop)
			{
				Stop();
				_pBuffer->SetCurrentPosition(0);
				_pBuffer->Play(0, 0, (( bLoop )? DSBPLAY_LOOPING: 0));
			}

			//---------------------------------------------------------------------------
			//!	停止
			//---------------------------------------------------------------------------
			void	Stop(void)
			{
				_pBuffer->Stop();
			}

			//---------------------------------------------------------------------------
			//!	音量変更
			//!	@param iVolume [in] 音量(0〜255)
			//---------------------------------------------------------------------------
			void	SetVolume(int iVolume)
			{
				iVolume = Clamp(iVolume, 0, 255) - 255;
				iVolume = iVolume * iVolume * 100 / (255 * 255);
				_pBuffer->SetVolume(iVolume * -iVolume);
			}

			//---------------------------------------------------------------------------
			//!	パン変更
			//!	@param iPan [in] パン
			//---------------------------------------------------------------------------
			void	SetPan(int iPan)
			{
				_pBuffer->SetPan(iPan);
			}

			//---------------------------------------------------------------------------
			//!	再生速度変更
			//!	@param fSpeed [in] 再生速度
			//---------------------------------------------------------------------------
			void	SetSpeed(float fSpeed)
			{
				int iFrequency = (int)((float)_iRate * fSpeed);
				_pBuffer->SetFrequency(iFrequency);
			}

			//---------------------------------------------------------------------------
			//!	再生されているか調べる
			//!	@return 再生フラグ
			//---------------------------------------------------------------------------
			bool	IsPlay(void)
			{
				dword dwAns;
				_pBuffer->GetStatus(&dwAns);
				return (dwAns & DSBSTATUS_PLAYING)? true: false;
			}

		private:
			//---------------------------------------------------------------------------
			//!	WAVEの読み込み
			//!	@param pFileName [in] ファイル名
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	LoadWave(char* pFileName)
			{
				char cTemp[256];
				HMMIO	hmmio;
				bool	bResult = false;

				MMCKINFO		mainChunk;
				MMCKINFO		subChunk;
				WAVEFORMATEX	wfx;
				byte*			pBuffer = null;

				DSBUFFERDESC    dsbd;

				//	ファイルオープン
				hmmio = mmioOpen(pFileName, null, MMIO_ALLOCBUF | MMIO_READ);
				if ( hmmio == null )
				{
					sprintf(cTemp, "指定されたWAVEファイルが存在しません(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}

				//---------------------------------------------------------------------------
				//	WAVEファイルの読み込み
				//---------------------------------------------------------------------------
				//	RIFFチャンクのWAVEタイプ検索
				mainChunk.fccType = mmioFOURCC('W','A', 'V', 'E');
				if( mmioDescend(hmmio, &mainChunk, null, MMIO_FINDRIFF) )
				{
					sprintf(cTemp, "読み込んだファイルはWAVEファイルではありません(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}

				//	fmtチャンクの検索
				subChunk.ckid = mmioFOURCC('f', 'm', 't', ' ');
				if( mmioDescend(hmmio, &subChunk, &mainChunk, MMIO_FINDCHUNK) )
				{
					sprintf(cTemp, "読み込んだWAVEファイルは壊れています(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}

				//	フォーマットの読み込み
				if( mmioRead(hmmio, (HPSTR)&wfx, sizeof(wfx)) != sizeof(wfx) )
				{
					sprintf(cTemp, "読み込んだWAVEファイルのフォーマットが壊れているか、対応していません(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}
				//	フォーマットの確認
				if( wfx.wFormatTag != WAVE_FORMAT_PCM )
				{
					sprintf(cTemp, "読み込んだWAVEファイルのフォーマットが対応してません(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}
				wfx.cbSize = 0;

				//	チャンクを戻る
				if( mmioAscend(hmmio, &subChunk, 0) )
				{
					sprintf(cTemp, "WAVEファイルの読み込み中、チャンクから戻るのに失敗しました(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}

				//	dataチャンクを検索
				subChunk.ckid = mmioFOURCC('d', 'a', 't', 'a');
				if( mmioDescend(hmmio, &subChunk, &mainChunk, MMIO_FINDCHUNK) )
				{
					sprintf(cTemp, "読み込んだWAVEファイルのデータが壊れています(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}

				//	メモリ確保
				pBuffer = new byte[subChunk.cksize];
				//	バッファの読み込み
				if( (dword)mmioRead(hmmio, (HPSTR)pBuffer, subChunk.cksize) == subChunk.cksize )
				{
					//	セカンダリバッファ情報の設定
					ZeroMemory(&dsbd, sizeof(DSBUFFERDESC));

					dsbd.dwSize	 = sizeof(DSBUFFERDESC);
					dsbd.dwFlags = DSBCAPS_STATIC |			// サウンドデータがスタティック(ストリームではない)
								   DSBCAPS_CTRLVOLUME |		// ボリュームコントロール可能
								   DSBCAPS_CTRLPAN |		// パンコントロール可能
								   DSBCAPS_CTRLFREQUENCY |	// 周波数コントロール有効
								   DSBCAPS_GLOBALFOCUS;		// 非アクティブ時も音を出す
					dsbd.dwBufferBytes = subChunk.cksize;
					dsbd.lpwfxFormat   = &wfx;

					_iRate = wfx.nSamplesPerSec;

					//	セカンダリバッファの生成
					bResult = CreateSecondaryBuffer(pBuffer, &dsbd);
				}

				SafeDeleteArray(pBuffer);

				//	ファイルを閉じる
				mmioClose(hmmio, 0);

				return bResult;
			}

			//---------------------------------------------------------------------------
			//!	OGGの読み込み
			//!	@param pFileName [in] ファイル名
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	LoadOgg(char* pFileName)
			{
				HANDLE hFile;
				OggVorbis_File vf;

				DSBUFFERDESC dsbd;
				WAVEFORMATEX wfx;

				byte* pBuffer;
				char* pAsd;
				dword dwActualRead;

				//	ファイルを読み込む
				hFile = CreateFile(pFileName, GENERIC_READ, FILE_SHARE_READ, (LPSECURITY_ATTRIBUTES)null, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, null);
				if( hFile == (HANDLE)-1 )
					return false;

				//	Oggファイルを開く
				ov_callbacks oggCallbacks =	{ CallbackRead, CallbackSeek, CallbackClose, CallbackTell };
				if( ov_open_callbacks(hFile, &vf, null, 0, oggCallbacks) )
				{
					CloseHandle(hFile);
					char cTemp[256];
					sprintf(cTemp, "Oggの読み込みに失敗しました(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}

				//	情報設定
				const vorbis_info* info = ov_info(&vf, -1);
				if( info == null ) ov_clear(&vf);

				//	WAVEフォーマット初期化
				ZeroMemory(&wfx, sizeof(WAVEFORMATEX));
				wfx.wFormatTag		= WAVE_FORMAT_PCM;
				wfx.nChannels		= info->channels;
				wfx.nSamplesPerSec	= info->rate;
				wfx.wBitsPerSample	= 16;

				wfx.nBlockAlign		= 4;
				wfx.nAvgBytesPerSec	= info->rate * 4;

				_iRate = info->rate;

				//	セカンダリバッファ情報の設定
				ZeroMemory(&dsbd, sizeof(DSBUFFERDESC));
				dsbd.dwSize  = sizeof(DSBUFFERDESC);
				dsbd.dwFlags = DSBCAPS_STATIC |			// サウンドデータがスタティック(ストリームではない)
							   DSBCAPS_CTRLVOLUME |		// ボリュームコントロール可能
							   DSBCAPS_CTRLPAN |		// パンコントロール可能
							   DSBCAPS_CTRLFREQUENCY |	// 周波数コントロール有効
							   DSBCAPS_GLOBALFOCUS;		// 非アクティブ時も音を出す
				dsbd.dwBufferBytes = (dword)(ov_pcm_total(&vf, -1) * info->channels * (wfx.wBitsPerSample / 8));
				dsbd.lpwfxFormat = &wfx;

				//	メモリ確保
				pBuffer = new byte[dsbd.dwBufferBytes];
				//	OggファイルをWaveデータに変換
				pAsd = (char*)pBuffer;
				do
				{
					dwActualRead = ov_read(&vf, pAsd, 4096, 0, 2, 1, null);
					pAsd += dwActualRead;
				}
				while( dwActualRead != 0 );

				//	セカンダリバッファの生成
				CreateSecondaryBuffer(pBuffer, &dsbd);

				SafeDeleteArray(pBuffer);
				ov_clear(&vf);

				return true;
			}

			//---------------------------------------------------------------------------
			//!	セカンドバッファの生成
			//!	@param pBuffer [in] データ
			//!	@param pDsbd [in] バッファデスク
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	CreateSecondaryBuffer(byte* pBuffer, LPDSBUFFERDESC pDsbd)
			{
				HRESULT hr;

				void* pQueue1;
				void* pQueue2;
				dword dwQueue1, dwQueue2;

				//	セカンダリバッファの生成
				hr = pDSound->CreateSoundBuffer(pDsbd, &_pBuffer, null);
				if( SUCCEEDED(hr) )
				{
					//	セカンダリバッファのロック
					_pBuffer->Lock(0, pDsbd->dwBufferBytes, &pQueue1, &dwQueue1, &pQueue2, &dwQueue2, 0);
					//	サウンドキュー(リングバッファ)へデータをコピー
					memcpy(pQueue1, pBuffer, dwQueue1);
					//	回り込んでいる部分へ残りのデータをコピー
					if( pQueue2 )
						memcpy(pQueue2, pBuffer + dwQueue1, dwQueue2);
					//	セカンダリバッファのロック解除
					_pBuffer->Unlock(pQueue1, dwQueue1, pQueue2, dwQueue2);
					return true;
				}
				return false;
			}

			LPDIRECTSOUNDBUFFER	_pBuffer;	//!< 二次バッファ

			float	_fSpeed;	//!< 速度
			int		_iRate;		//!< レート
		};

		//===========================================================================
		//!	ストリームサウンドクラス
		//===========================================================================
		class StreamSound
			: public StreamBase
		{
		public:
			//---------------------------------------------------------------------------
			//!	コンストラクタ
			//---------------------------------------------------------------------------
			StreamSound(void)
				: _pBuffer(null), _bFlag(false)
				, _bEnd(true)
			{
			}

			//---------------------------------------------------------------------------
			//!	コンストラクタ
			//---------------------------------------------------------------------------
			~StreamSound(void)
			{
				if( _pBuffer )
				{
					_bEnd = true;
					if( !_StreamThread.IsEnd() )
						_StreamThread.Shutdown(true);
					if( _bType == TYPE_OGG )
						ov_clear(&_vf);
					else
						CloseHandle(_hFile);
					SafeRelease(_pBuffer);
				}
			}

			//---------------------------------------------------------------------------
			//!	読み込み
			//!	@param pFileName [in] ファイル名
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	Load(char* pFileName)
			{
				dword i;
				//	拡張子取得
				for(i = (dword)strlen(pFileName); i > 0; i--)
					if( pFileName[i - 1] == '.' )
						break;

				bool bResult = false;
				//	WAVEの読み込み
				if( strcmp(&pFileName[i], "wav") == 0 )
					bResult = SetWave(pFileName);
				//	OGGの読み込み
				if( strcmp(&pFileName[i], "ogg") == 0 )
					bResult = SetOgg(pFileName);

				if( bResult == false )
					return false;

				//	初期設定
				_pBuffer->SetCurrentPosition(0);
				SetVolume((_iMode == EStreamMode::FADEIN)? 0: 255);
				SetPan(0);
				_bEnd = false;

				//	ストリーム管理スレッド起動
				_StreamThread.Luncher(StreamFunction, this);

				//	再生
				_pBuffer->Play(0, 0, DSBPLAY_LOOPING);

				return true;
			}

			//---------------------------------------------------------------------------
			//!	停止
			//---------------------------------------------------------------------------
			void	Play(void)
			{
				_pBuffer->Play(0, 0, DSBPLAY_LOOPING);
			}

			//---------------------------------------------------------------------------
			//!	停止
			//---------------------------------------------------------------------------
			void	Stop(void)
			{
				_pBuffer->Stop();
			}

			//---------------------------------------------------------------------------
			//!	再生フラグ取得
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	IsPlay(void)
			{
				dword dwAns;
				_pBuffer->GetStatus(&dwAns);
				return (dwAns & DSBSTATUS_PLAYING)? true: false;
			}

			//---------------------------------------------------------------------------
			//!	更新
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	Update(void)
			{
				dword dwPointer;
				dword dwSize = (dword)(_iRate * 4);
				//	現在の再生位置取得
				_pBuffer->GetCurrentPosition(&dwPointer, null);
				//	読み込み開始判定
				if( (_bFlag && dwPointer < dwSize) ||
					(!_bFlag && dwPointer >= dwSize) )
				{
					if( _bType == TYPE_WAVE )
						SetWaveBuffer(dwSize);
					if( _bType == TYPE_OGG )
						SetOggBuffer(dwSize);

					_bFlag = !_bFlag;

					return true;
				}

				//	フェードイン
				if( _iMode == EStreamMode::FADEIN )
				{
					SetVolume(_iVolume + _iParam);
					if( _iVolume == 255 )
						_iMode = EStreamMode::NORMAL;
				}
				//	フェードアウト
				if( _iMode == EStreamMode::FADEOUT )
				{
					SetVolume(_iVolume - _iParam);
					if( _iVolume == 0 )
					{
						_iMode = EStreamMode::NORMAL;
						Stop();
					}
				}

				return false;
			}

			//---------------------------------------------------------------------------
			//!	音量のセット
			//!	@param iVolume [in] 音量(0〜255)
			//---------------------------------------------------------------------------
			void	SetVolume(int iVolume)
			{
				_iVolume = Clamp(iVolume, 0, 255);
				iVolume = _iVolume - 255;
				iVolume = iVolume * iVolume * 100 / (255 * 255);
				_pBuffer->SetVolume(iVolume * -iVolume);
			}

			//---------------------------------------------------------------------------
			//!	パンのセット
			//!	@param iPan [in] パン
			//---------------------------------------------------------------------------
			void	SetPan(int iPan)
			{
				_pBuffer->SetPan(iPan);
			}

			//---------------------------------------------------------------------------
			//! モードセット
			//!	@param iMode [in] モード
			//---------------------------------------------------------------------------
			void	SetMode(int iMode)
			{
				_iMode = iMode;
			}

			//---------------------------------------------------------------------------
			//! パラメータセット
			//!	@param iParam [in] パラメータ
			//---------------------------------------------------------------------------
			void	SetParam(int iParam)
			{
				_iParam = iParam;
			}

			//---------------------------------------------------------------------------
			//!	終了フラグの取得
			//!	@return 終了フラグ
			//---------------------------------------------------------------------------
			bool	GetEnd(void)
			{
				return _bEnd;
			}

			//---------------------------------------------------------------------------
			//!	バッファの初期化
			//!	@param wfx [in] Waveフォーマット
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	InitBuffer(WAVEFORMATEX wfx)
			{
				//	レートセット
				_iRate = wfx.nSamplesPerSec;

				DSBUFFERDESC	dsbd;
				//	セカンダリバッファ情報の設定
				ZeroMemory(&dsbd, sizeof(DSBUFFERDESC));
				dsbd.dwSize	 = sizeof(DSBUFFERDESC);
				dsbd.dwFlags = DSBCAPS_CTRLVOLUME |		// ボリュームコントロール可能
							   DSBCAPS_CTRLPAN |		// パンコントロール可能
							   DSBCAPS_CTRLFREQUENCY |	// 周波数コントロール有効
							   DSBCAPS_GLOBALFOCUS;		// 非アクティブ時も音を出す
				dsbd.dwBufferBytes = _iRate * 4 * 2;
				dsbd.lpwfxFormat   = &wfx;

				//	セカンダリバッファの生成
				if( FAILED(pDSound->CreateSoundBuffer(&dsbd, &_pBuffer, null)) )
					return false;
				_pBuffer->SetFormat(&wfx);

				_bFlag = true;

				return true;
			}

			//---------------------------------------------------------------------------
			//!	Waveのセット
			//!	@param pFileName [in] ファイル名
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	SetWave(char* pFileName)
			{
				char cTemp[256];
				HMMIO	hmmio;

				MMCKINFO		mainChunk;
				MMCKINFO		subChunk;
				WAVEFORMATEX	wfx;
				byte*			pBuffer = null;

				// mmioファイルオープン
				hmmio = mmioOpen(pFileName, null, MMIO_ALLOCBUF | MMIO_READ);
				if ( hmmio == null )
				{
					sprintf(cTemp, "指定されたWAVEファイルが存在しません(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}

				//---------------------------------------------------------------------------
				//	WAVEファイルの読み込み
				//---------------------------------------------------------------------------
				//	RIFFチャンクのWAVEタイプ検索
				mainChunk.fccType = mmioFOURCC('W','A', 'V', 'E');
				if( mmioDescend(hmmio, &mainChunk, null, MMIO_FINDRIFF) )
				{
					sprintf(cTemp, "読み込んだファイルはWAVEファイルではありません(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}
				//	fmtチャンクの検索
				subChunk.ckid = mmioFOURCC('f', 'm', 't', ' ');
				if( mmioDescend(hmmio, &subChunk, &mainChunk, MMIO_FINDCHUNK) )
				{
					sprintf(cTemp, "読み込んだWAVEファイルは壊れています(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}
				//	フォーマットの読み込み
				if( mmioRead(hmmio, (HPSTR)&wfx, sizeof(wfx)) != sizeof(wfx) )
				{
					sprintf(cTemp, "読み込んだWAVEファイルのフォーマットが壊れているか、対応していません(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}
				//	フォーマットの確認
				if( wfx.wFormatTag != WAVE_FORMAT_PCM )
				{
					sprintf(cTemp, "読み込んだWAVEファイルのフォーマットが対応してません(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}
				wfx.cbSize = 0;
				//	チャンクを戻る
				if( mmioAscend(hmmio, &subChunk, 0) )
				{
					sprintf(cTemp, "WAVEファイルの読み込み中、チャンクから戻るのに失敗しました(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}
				//	dataチャンクを検索
				subChunk.ckid = mmioFOURCC('d', 'a', 't', 'a');
				if( mmioDescend(hmmio, &subChunk, &mainChunk, MMIO_FINDCHUNK) )
				{
					sprintf(cTemp, "読み込んだWAVEファイルのデータが壊れています(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}

				//	現在位置取得
				_dwBegin = mmioSeek(hmmio, 0, SEEK_CUR);
				//	ファイルを閉じる
				mmioClose(hmmio, 0);

				//	ファイルを読み込む
				_hFile = CreateFile(pFileName, GENERIC_READ, FILE_SHARE_READ, (LPSECURITY_ATTRIBUTES)null, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, null);
				if( _hFile == (HANDLE)-1 )
					return false;

				//	開始位置セット
				SetFilePointer(_hFile, _dwBegin, null, FILE_BEGIN);

				_bType = TYPE_WAVE;

				//	バッファの生成
				return InitBuffer(wfx);
			}

			//---------------------------------------------------------------------------
			//!	Oggのセット
			//!	@param pFileName [in] ファイル名
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	SetOgg(char* pFileName)
			{
				//	ファイルを読み込む
				_hFile = CreateFile(pFileName, GENERIC_READ, FILE_SHARE_READ, (LPSECURITY_ATTRIBUTES)null, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, null);
				if( _hFile == (HANDLE)-1 )
					return false;

				//	Oggファイルを開く
				ov_callbacks oggCallbacks =	{ CallbackRead, CallbackSeek, CallbackClose, CallbackTell };
				if( ov_open_callbacks(_hFile, &_vf, null, 0, oggCallbacks) )
				{
					CloseHandle(_hFile);
					char cTemp[256];
					sprintf(cTemp, "Oggの読み込みに失敗しました(%s)", pFileName);
					Message(cTemp, "エラー");
					return false;
				}

				//	情報設定
				const vorbis_info* info = ov_info(&_vf, -1);
				if( info == null ) ov_clear(&_vf);

				WAVEFORMATEX wfx;
				//	WAVEフォーマット初期化
				ZeroMemory(&wfx, sizeof(WAVEFORMATEX));
				wfx.wFormatTag		= WAVE_FORMAT_PCM;
				wfx.nChannels		= info->channels;
				wfx.nSamplesPerSec	= info->rate;
				wfx.wBitsPerSample	= 16;

				wfx.nBlockAlign		= 4;
				wfx.nAvgBytesPerSec	= info->rate * 4;

				_bType = TYPE_OGG;

				return InitBuffer(wfx);
			}

			//---------------------------------------------------------------------------
			//!	Waveバッファのセット
			//!	@param iBufferSize [in] セットするバッファサイズ
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	SetWaveBuffer(int iBufferSize)
			{
				byte* pQueue1;
				byte* pQueue2;
				dword dwQueue1, dwQueue2;
				dword dwSize;

				//	ロック
				_pBuffer->Lock(iBufferSize * _bFlag, iBufferSize, (void**)&pQueue1, &dwQueue1, (void**)&pQueue2, &dwQueue2, 0);

				//	Waveデータ読み込み
				ReadFile(_hFile, pQueue1, dwQueue1, &dwSize, null);
				if( dwSize < dwQueue1 )
				{
					//	開始位置に戻す
					SetFilePointer(_hFile, _dwBegin , null, FILE_BEGIN);
					ReadFile(_hFile, pQueue1 + dwSize, dwQueue1 - dwSize, &dwSize, null);
				}

				//	Waveデータ読み込み
				ReadFile(_hFile, pQueue2, dwQueue2, &dwSize, null);
				if( dwSize < dwQueue2 )
				{
					//	開始位置に戻す
					SetFilePointer(_hFile, _dwBegin , null, FILE_BEGIN);
					ReadFile(_hFile, pQueue2 + dwSize, dwQueue2 - dwSize, &dwSize, null);
				}

				//	アンロック
				_pBuffer->Unlock(pQueue1, dwQueue1, pQueue2, dwQueue2);

				return false;
			}

			//---------------------------------------------------------------------------
			//!	Oggバッファのセット
			//!	@param iBufferSize [in] セットするバッファサイズ
			//!	@return 結果
			//---------------------------------------------------------------------------
			bool	SetOggBuffer(int iBufferSize)
			{
				void* pQueue1;
				void* pQueue2;
				dword dwQueue1, dwQueue2;

				//	ロック
				_pBuffer->Lock(iBufferSize * _bFlag, iBufferSize, &pQueue1, &dwQueue1, &pQueue2, &dwQueue2, 0);

				//	OggファイルをWaveデータに変換
				SetOggBuffer(pQueue1, dwQueue1);
				if( pQueue2 )
					SetOggBuffer(pQueue2, dwQueue2);

				//	アンロック
				_pBuffer->Unlock(pQueue1, dwQueue1, pQueue2, dwQueue2);

				return true;
			}

			//---------------------------------------------------------------------------
			//!	Oggデータ変換
			//!	@param pBuffer [out] データの受け皿
			//!	@param dwSize [in] サイズ
			//---------------------------------------------------------------------------
			bool	SetOggBuffer(void* pBuffer, dword dwSize)
			{
				char* pAsd;
				long dwActualRead;

				//	OggファイルをWaveデータに変換
				pAsd = (char*)pBuffer;
				while( dwSize > 0 )
				{
					dwActualRead = ov_read(&_vf, pAsd, dwSize, 0, 2, 1, null);

					dwSize -= dwActualRead;
					pAsd += dwActualRead;
					if( dwActualRead <= 0 )
					{
						ov_pcm_seek(&_vf, 0);
					}
				}

				return true;
			}

		private:
			Lib::Thread::Thread	_StreamThread;	//!< 管理スレッド
			LPDIRECTSOUNDBUFFER	_pBuffer;		//!< 二次バッファ

			HANDLE			_hFile;		//!< ファイルハンドル
			OggVorbis_File	_vf;		//!< Ogg読み込み用
			dword			_dwBegin;	//!< データ開始位置
			int				_iMode;		//!< 再生モード
			int				_iParam;	//!< パラメータ
			byte			_bType;		//!< ファイルタイプ
			bool			_bFlag;		//!< 書き込み位置変更フラグ
			bool			_bEnd;		//!< 終了フラグ
			int				_iVolume;	//!< 音量
			int				_iRate;		//!< レート
		};


		//---------------------------------------------------------------------------
		//	ストリーム再生管理
		//!	@param pArg [in] ストリームクラスのアドレス
		//!	@return 結果
		//---------------------------------------------------------------------------
		unsigned int __stdcall	StreamFunction(void* pArg)
		{
			StreamSound* pStream = (StreamSound*)pArg;

			//	管理用ループ
			while( true )
			{
				//	終了確認
				if( pStream->GetEnd() )
					break;
				Sleep(100);
				pStream->Update();
			}

			return 0;
		}

		//---------------------------------------------------------------------------
		//	ストリーム再生クラス生成
		//!	@param pFileName [in] ファイル名
		//!	@param ppStream [out] ストリーム再生クラスポインタ
		//!	@param iMode [in] 再生モード
		//!	@param iParam [in] パラメータ
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Create(char* pFileName, StreamBase** ppStream, int iMode, int iParam)
		{
			StreamSound* pNewStream = new StreamSound;
			*ppStream = pNewStream;
			pNewStream->SetMode(iMode);
			pNewStream->SetParam(iParam);
			return pNewStream->Load(pFileName);
		}

		//---------------------------------------------------------------------------
		//	ストリーム再生クラス解放
		//!	@param ppStream [out] ストリーム再生クラスポインタ
		//---------------------------------------------------------------------------
		void	Delete(StreamBase** ppStream)
		{
			SafeDelete(*ppStream);
		}

		//! 二次バッファ
		std::map<dword, SoundBuffer*> pBuffer;

		//---------------------------------------------------------------------------
		//!	初期化
		//!	@param hWnd [in] ウィンドウハンドル
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Init(HWND hWnd)
		{
			HRESULT hr;

			// COMの初期化
			CoInitialize(null);
			//	サウンドオブジェクトの生成
			hr = DirectSoundCreate(null, &pDSound, null);
			if( FAILED(hr) )
				return false;
			//	協調レベルの設定
			hr = pDSound->SetCooperativeLevel(hWnd, DSSCL_PRIORITY);
			if( FAILED(hr) )
				return false;

			DSBUFFERDESC dsbdesc;
			//	DSBUFFERDESCの設定
			ZeroMemory(&dsbdesc, sizeof(DSBUFFERDESC));
			dsbdesc.dwSize	= sizeof(DSBUFFERDESC);
			dsbdesc.dwFlags	= DSBCAPS_CTRLVOLUME | DSBCAPS_PRIMARYBUFFER;

			//	一次バッファの生成
			hr = pDSound->CreateSoundBuffer(&dsbdesc, &pPrimary, null);
			if( FAILED(hr) )
				return false;

			return true;
		}

		//---------------------------------------------------------------------------
		//!	解放
		//---------------------------------------------------------------------------
		void	Cleanup(void)
		{
			CleanupAll();
			SafeRelease(pPrimary);
			SafeRelease(pDSound);
		}

		//---------------------------------------------------------------------------
		//	読み込み
		//!	@param dwNo [in] 番号
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Load(dword dwNo, char* pFileName)
		{
			Cleanup(dwNo);
			//	バッファの生成
			pBuffer[dwNo] = new SoundBuffer;

			return pBuffer[dwNo]->Load(pFileName);
		}

		//---------------------------------------------------------------------------
		//	解放
		//!	@param dwNo [in] 番号
		//---------------------------------------------------------------------------
		void	Cleanup(dword dwNo)
		{
			std::map<dword, SoundBuffer*>::iterator it;
			it = pBuffer.find(dwNo);
			if( it != pBuffer.end() )
			{
				SafeDelete(pBuffer[dwNo]);
				pBuffer.erase(it);
			}
		}

		//---------------------------------------------------------------------------
		//	解放
		//---------------------------------------------------------------------------
		void	CleanupAll(void)
		{
			while( pBuffer.size() )
				Cleanup(pBuffer.begin()->first);
		}

		//---------------------------------------------------------------------------
		//	再生
		//!	@param dwNo [in] 番号
		//!	@param bLoop [in] ループフラグ
		//---------------------------------------------------------------------------
		void	Play(dword dwNo, bool bLoop)
		{
			pBuffer[dwNo]->Play(bLoop);
		}

		//---------------------------------------------------------------------------
		//	停止
		//!	@param dwNo [in] 番号
		//---------------------------------------------------------------------------
		void	Stop(dword dwNo)
		{
			pBuffer[dwNo]->Stop();
		}

		//---------------------------------------------------------------------------
		//	停止
		//---------------------------------------------------------------------------
		void	Stop(void)
		{
			std::map<dword, SoundBuffer*>::iterator it;
			it = pBuffer.begin();
			for(; it != pBuffer.end(); it++)
				Stop(it->first);
		}

		//---------------------------------------------------------------------------
		//	再生フラグ取得
		//!	@param dwNo [in] 番号
		//!	@return 再生フラグ
		//---------------------------------------------------------------------------
		bool	IsPlay(dword dwNo)
		{
			return pBuffer[dwNo]->IsPlay();
		}

		//---------------------------------------------------------------------------
		//	音量変更
		//!	@param dwNo [in] 番号
		//!	@param iVolume [in] 音量
		//---------------------------------------------------------------------------
		void	SetVolume(dword dwNo, int iVolume)
		{
			pBuffer[dwNo]->SetVolume(iVolume);
		}

		//---------------------------------------------------------------------------
		//	パン変更
		//!	@param dwNo [in] 番号
		//!	@param iPan [in] パン
		//---------------------------------------------------------------------------
		void	SetPan(dword dwNo, int iPan)
		{
			pBuffer[dwNo]->SetPan(iPan);
		}

		//---------------------------------------------------------------------------
		//	再生速度変更
		//!	@param dwNo [in] 番号
		//!	@param fSpeed [in] 再生速度
		//---------------------------------------------------------------------------
		void	SetSpeed(dword dwNo, float fSpeed)
		{
			pBuffer[dwNo]->SetSpeed(fSpeed);
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================