//---------------------------------------------------------------------------
//!
//!	@file	Lib.Math.h
//!	@brief	計算関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace Math
	{
		//!	初期シードの設定
		void	Seed(unsigned int iSeed);
		//!	乱数取得
		int		Random(void);
		//!	乱数取得
		int		Random(int iMin, int iMax);
		//!	乱数取得
		double	Random(double dMin, double dMax);

		//!	長さを2乗した数の取得
		float	Length2(const Vector3& Vec);
		//!	長さの取得
		float	Length(const Vector3& Vec);
		//!	長さの2乗した数の取得
		float	Length2(const Vector2& Vec);
		//!	長さの取得
		float	Length(const Vector2& Vec);

		//!	正規化
		void	Normalize(Vector3* pOut, const Vector3& vec);
		//!	内積
		float	Dot(const Vector3& Vec1, const Vector3& Vec2);
		//!	外積による2つの直線に垂直な線の取得
		void	Cross(Vector3* pVec, const Vector3& Vec1, const Vector3& Vec2);

		//!	初期化
		void	Identity(Matrix* pOut);
		//!	座標変換行列作成
		void	Translation(Matrix* pOut, const Vector3& Posisition);
		//!	X軸回りの回転行列作成
		void	RotationX(Matrix* pOut, float fAngle);
		//!	Y軸回りの回転行列作成
		void	RotationY(Matrix* pOut, float fAngle);
		//!	Z軸回りの回転行列作成
		void	RotationZ(Matrix* pOut, float fAngle);
		//!	XYZ軸回りの回転行列作成
		void	RotationXYZ(Matrix* pOut, const Vector3& Rotate);
		//!	スケール変換行列作成
		void	Scaling(Matrix* pOut, float fScale);
		//!	スケール変換行列作成
		void	Scaling(Matrix* pOut, const Vector3& Scale);
		//!	任意軸回転行列作成
		void	RotationAxis(Matrix* pOut, const Vector3& Axis, float fAngle);
		//!	1つ目のベクトルから2つ目のベクトルに変換する行列作成
		void	RotationVector(Matrix* pOut, const Vector3& Vec1, const Vector3& Vec2);
	}
}

//============================================================================
//	END OF FILE
//============================================================================