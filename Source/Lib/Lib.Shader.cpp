//---------------------------------------------------------------------------
//!
//!	@file	Lib.Shader.cpp
//!	@brief	シェーダー
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace Shader
	{
		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Shader::Shader(void)
			: _pEffect(null), _uPass(0)
		{
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		Shader::~Shader(void)
		{
		}

		//---------------------------------------------------------------------------
		//	初期化
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Shader::Init(const char* pFileName)
		{
			SafeRelease(_pEffect);

			HRESULT hr;
			LPD3DXBUFFER pErr = null;

			//	エフェクトの生成
			hr = D3DXCreateEffectFromFile(	Lib::System::System::GetInstance()->GetDevice(),
											pFileName,
											null,
											null,
											D3DXSHADER_DEBUG,
											null,
											&_pEffect,
											&pErr );

			if( FAILED(hr) )
				Message((char*)pErr->GetBufferPointer(), "Shader読み込みエラー");

			SafeRelease(pErr);
			_uPass = 0;
			return SUCCEEDED(hr);
		}

		//---------------------------------------------------------------------------
		//	解放
		//---------------------------------------------------------------------------
		void	Shader::Cleanup(void)
		{
			SafeRelease(_pEffect);
		}

		//---------------------------------------------------------------------------
		//	コミットチェンジ
		//---------------------------------------------------------------------------
		void	Shader::CommitChanges(void)
		{
			_pEffect->CommitChanges();
		}

		//---------------------------------------------------------------------------
		//	シェーダーの開始
		//---------------------------------------------------------------------------
		void	Shader::BeginShader(void)
		{
			_pEffect->SetTechnique(_Technique);
			_pEffect->Begin(&_uPass, 0);
		}

		//---------------------------------------------------------------------------
		//	シェーダーの終了
		//---------------------------------------------------------------------------
		void	Shader::EndShader(void)
		{
			_pEffect->End();
		}

		//---------------------------------------------------------------------------
		//	パスの開始
		//---------------------------------------------------------------------------
		void	Shader::BeginPass(unsigned int uPass)
		{
			_pEffect->BeginPass(uPass);
		}

		//---------------------------------------------------------------------------
		//	パスの終了
		//---------------------------------------------------------------------------
		void	Shader::EndPass(void)
		{
			_pEffect->EndPass();
		}

		//---------------------------------------------------------------------------
		//	パスの数の取得
		//!	@return パス数
		//---------------------------------------------------------------------------
		unsigned	Shader::GetPass(void)
		{
			return _uPass;
		}

		//---------------------------------------------------------------------------
		//	テクニックのセット
		//!	@param pTechnique [in] テクニック
		//---------------------------------------------------------------------------
		void	Shader::SetTechnique(char* pTechnique)
		{
			_Technique = _pEffect->GetTechniqueByName(pTechnique);
		}

		//---------------------------------------------------------------------------
		//	Matrixのセット
		//!	@param pName [in] 名称
		//!	@param pTexture [in] テクスチャー
		//---------------------------------------------------------------------------
		void	Shader::SetTexture(char* pName, const LPDIRECT3DTEXTURE9 pTexture)
		{
			_pEffect->SetTexture(pName, pTexture);
		}

		//---------------------------------------------------------------------------
		//	Matrixのセット
		//!	@param pName [in] 名称
		//!	@param Param [in] 値
		//---------------------------------------------------------------------------
		void	Shader::SetMatrix(char* pName, const Matrix& Param)
		{
			_pEffect->SetMatrix(pName, &Param);
		}

		//---------------------------------------------------------------------------
		//	intのセット
		//!	@param pName [in] 名称
		//!	@param Param [in] 値
		//---------------------------------------------------------------------------
		void	Shader::SetInt(char* pName, int Param)
		{
			_pEffect->SetInt(pName, Param);
		}

		//---------------------------------------------------------------------------
		//	intのセット
		//!	@param pName [in] 名称
		//!	@param Param [in] 値
		//!	@param iNum [in] 数
		//---------------------------------------------------------------------------
		void	Shader::SetIntArray(char* pName, const int* pParam, dword dwNum)
		{
			_pEffect->SetIntArray(pName, pParam, dwNum);
		}

		//---------------------------------------------------------------------------
		//	floatのセット
		//!	@param pName [in] 名称
		//!	@param Param [in] 値
		//---------------------------------------------------------------------------
		void	Shader::SetFloat(char* pName, float Param)
		{
			_pEffect->SetFloat(pName, Param);
		}

		//---------------------------------------------------------------------------
		//	floatのセット
		//!	@param pName [in] 名称
		//!	@param Param [in] 値
		//!	@param iNum [in] 数
		//---------------------------------------------------------------------------
		void	Shader::SetFloatArray(char* pName, const float* pParam, dword dwNum)
		{
			_pEffect->SetFloatArray(pName, pParam, dwNum);
		}

		//---------------------------------------------------------------------------
		//	Vecot3のセット
		//!	@param pName [in] 名称
		//!	@param Param [in] 値
		//---------------------------------------------------------------------------
		void	Shader::SetVector3(char* pName, const Vector3& Param)
		{
			_pEffect->SetFloatArray(pName, (float*)&Param, 3);
		}

		//---------------------------------------------------------------------------
		//	Vecot3のセット
		//!	@param pName [in] 名称
		//!	@param Param [in] 値
		//!	@param iNum [in] 数
		//---------------------------------------------------------------------------
		void	Shader::SetVector3Array(char* pName, const Vector3* pParam, dword dwNum)
		{
			_pEffect->SetFloatArray(pName, (float*)pParam, 3 * dwNum);
		}

		//---------------------------------------------------------------------------
		//	floatのセット
		//!	@param pName [in] 名称
		//!	@param Param [in] 値
		//---------------------------------------------------------------------------
		void	Shader::SetVector4(char* pName, const Vector4& Param)
		{
			_pEffect->SetVector(pName, &Param);
		}

		//---------------------------------------------------------------------------
		//	floatのセット
		//!	@param pName [in] 名称
		//!	@param Param [in] 値
		//!	@param iNum [in] 数
		//---------------------------------------------------------------------------
		void	Shader::SetVector4Array(char* pName, const Vector4* pParam, dword dwNum)
		{
			_pEffect->SetVectorArray(pName, pParam, dwNum);
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================