//---------------------------------------------------------------------------
//!
//!	@file	Lib.Profile.h
//!	@brief	プロファイラー
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

#ifdef	_DEBUG

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace Profile
	{
		//!	ログの最大数
		const int LOG_MAX = 128;

		//!	プロファイルログ
		struct Log
		{
			Log*		pParent;		//!< 親
			Log*		pBrother;		//!< 兄弟
			Log*		pChiled;		//!< 子供

			// 計測用パラメータ
			double		dBeginTime;		//!< 計測開始時間
			double		dEndTime;		//!< 計測終了時間
			int			iTmpCount;		//!< 計測回数
			double		dTmpTotalTime;	//!< 合計処理時間
			double		dTmpMinTime;	//!< 最小処理時間
			double		dTmpMaxTime;	//!< 最大処理時間

			// 表示用パラメータ
			const char*	pName;			//!< 名前
			int			iCount;			//!< 計測回数
			double		dTotalTime;		//!< 合計処理時間
			double		dAverageTime;	//!< 平均処理時間
			double		dMinTime;		//!< 最小処理時間
			double		dMaxTime;		//!< 最大処理時間
		};

		//===========================================================================
		//!	プロファイラクラス
		//===========================================================================
		class Profiler
		{
		public:
			//! 初期化
			void	Init(void);
			//!	解放
			void	Cleanup(void);

			//!	フレームの開始をプロファイラに伝える
			void	Start(void);
			//!	計測開始
			void	Begin(const char* pName);
			//!	計測終了
			void	End(const char* pName);

			//!	ルートログの取得
			Log*	GetRootLog(void);

			//!	インスタンスの取得
			static	Profiler*	GetInstance(void);

		private:
			//!	コンストラクタ
			Profiler(void);
			//!	デストラクタ
			~Profiler(void);

			//!	ログの情報生成
			Log*	CreateLog(const char* pName, Log* pParent);
			//!	ログの更新7
			void	UpdateLog(Log* pLog);

		private:
			static Profiler	_Instance;	//!< 唯一のインスタンス

			Log*		_pRoot;			//!< ルートログ
			Time::Timer	_Timer;			//!< タイマー
			Log*		_pCurrent;		//!< カレントログ
			dword		_dwLogCount;	//!< ログの数
			int			_iTargetFPS;	//!< 目標フレームレート
		};

		//===========================================================================
		//!	スコープ内処理時間計測プロファイラ
		//===========================================================================
		class ScopeProfiler
		{
		public:
			//!	コンストラクタ
			ScopeProfiler(const char* pName);
			//!	デストラクタ
			~ScopeProfiler(void);

		private:
			const char*	_pName;	//!< プロファイル名
		};
	}
}

//	計測用マクロ
#define	PROFILER			Lib::Profile::Profiler::GetInstance()

#define START_PROFILE		PROFILER->Start()
#define BEGIN_PROFILE(name)	PROFILER->Begin(name)
#define END_PROFILE(name)	PROFILER->End(name)

#define	PROFILE(name)		Lib::Profile::ScopeProfiler ScopeProfiler(name)

#else // ~#if _DEBUG

#define	PROFILER
#define START_PROFILE
#define BEGIN_PROFILE(name)
#define END_PROFILE(name)

#define	PROFILE(name)

#endif // ~#if _DEBUG

//============================================================================
//	END OF FILE
//============================================================================