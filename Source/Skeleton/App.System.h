//---------------------------------------------------------------------------
//!
//!	@file	App.System.h
//!	@brief	アプリケーションシステム
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

// --------------------------------------------------------------------------
//	include
// --------------------------------------------------------------------------
#include "App.h"

namespace App
{
	namespace System
	{
		//===========================================================================
		//!	アプリケーションシステムクラス
		//===========================================================================
		class AppSystem
			: public Lib::System::System
		{
		public:
			//!	コンストラクタ
			AppSystem(void);
			//!	デストラクタ
			~AppSystem(void);

		private:
			//!	初期化
			bool	Init(void);
			//!	解放
			void	Cleanup(void);

			//!	更新
			void	Update(double dElapsedTime);
			//!	描画
			void	Render(double dElapsedTime);

		private:
#if	_DEBUG
			Lib::Frame::FPSCounter _FPSCounter;	//!< FPSカウンター
#endif	// ~#if _DEBUG
		};

		//!	ウインドウプロシージャー
		LRESULT	CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	}
}

//============================================================================
//	END OF FILE
//============================================================================