//===========================================================================
//!
//!	@file		App.Collision.h
//!	@brief		当たり判定関連
//!
//!	@author S.Kawamoto
//===========================================================================
#pragma once

namespace App
{
	namespace Collision
	{
		//===========================================================================
		//!	Objectクラス
		//===========================================================================
		class Object
		{
		public:
			//!	コンストラクタ
			Object(void);
			//!	デストラクタ
			~Object(void);

			//!	初期設定
			void	Init(int iShape, int iAttribute, int iHitAttribute = 0, bool bHitDelete = false);
			//!	当たりのセット
			void	SetObject(const Lib::Vector3& Pos, float fRadius, const Lib::Vector3& Pos2 = Lib::Vector3(0.0f, 0.0f, 0.0f));

			//!	衝突した位置の設定
			void	SetHitPos(const Lib::Vector3& Pos);
			//!	ステータスのセット
			void	SetState(int iState);

			//!	位置の取得
			Lib::Vector3	GetPos(void);
			//!	カプセル用の位置の取得
			Lib::Vector3	GetPos2(void);
			//!	衝突した位置の取得
			Lib::Vector3	GetHitPos(void);
			//!	半径の取得
			float	GetRadius(void);

			//!	状態の取得
			int		GetState(void);
			//!	形の取得
			int		GetShape(void);
			//!	属性の取得
			int		GetAttribute(void);
			//!	攻撃属性の取得
			int		GetHitAttribute(void);
			//!	消滅フラグの取得
			bool	GetHitDelete(void);

		private:
			Lib::Vector3 _Pos;		//!< 位置
			Lib::Vector3 _Pos2;		//!< カプセル型等の為の予備位置
			Lib::Vector3 _HitPos;	//!< 衝突した位置
			float	_fRadius;		//!< 半径
			int		_iState;		//!< 状態
			int		_iShape;		//!< 形
			int		_iAttribute;	//!< 属性
			int		_iHitAttribute;	//!< 攻撃用属性
			bool	_bHitDelete;	//!< 消滅フラグ
		};

		//===========================================================================
		//!	当たり判定クラス
		//===========================================================================
		class Collision
		{
		public:
			//	コンストラクタ
			Collision(void);
			//	デストラクタ
			~Collision(void);

			//!	解放
			void	Cleanup(void);
			//!	更新
			void	Update(void);

			//!	当たりオブジェクト追加
			void	SetObject(Object* pObject);

			//!	球と球の当たり判定
			bool	CollisionSphere(const Lib::Vector3& Pos1, float fRadius1, const Lib::Vector3& Pos2, float fRadius2, Lib::Vector3* pOut = null);
			//!	球とカプセルの当たり判定
			bool	CollisionCapsule(const Lib::Vector3& Pos, float fRadius1, const Lib::Vector3& Pos1, const Lib::Vector3& Pos2, float fRadius2, Lib::Vector3* pOut = null);

		private:
			//!	オブジェクトの当たり判定
			bool	CollisionObject(Object* pObject, Object* pAttack);

			std::vector<Object*>	_ObjectList;	//!< オブジェクトの当たり用リスト
			std::vector<Object*>	_AttackList;	//!< 攻撃の当たり用リスト
		};
	}
}

//============================================================================
//	END OF FILE
//============================================================================