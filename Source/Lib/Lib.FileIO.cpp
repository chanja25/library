//---------------------------------------------------------------------------
//!
//!	@file	Lib.FileIO.cpp
//!	@brief	入出力関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//	include
//---------------------------------------------------------------------------
#include "Lib.h"
#include <commdlg.h>

namespace Lib
{
	namespace FileIO
	{
		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		File::File(void)
			: _pBuffer(null)
			, _dwSize(0)
			, _bOpen(false)
			, _iFormat(0)
			, _dwOriginNum(0)
		{
			ZeroMemory(_cDirectory, sizeof(_cDirectory));
			ZeroMemory(_cPath, sizeof(_cPath));
			ZeroMemory(_cName, sizeof(_cName));
			ZeroMemory(_cExtension, sizeof(_cExtension));
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		File::~File(void)
		{
			CloseCheck();

			//	解放
			SafeDeleteArray(_pBuffer);
		}

		//---------------------------------------------------------------------------
		//	読み込み
		//!	@param pPath [in] パス
		//!	@return ファイルデータ
		//---------------------------------------------------------------------------
		void*	File::Read(const char* pPath)
		{
			CloseCheck();

			SafeDeleteArray(_pBuffer);

			//---------------------------------------------------------------------------
			//	ファイルを開く
			//---------------------------------------------------------------------------
			_hFile = CreateFile(pPath,
								GENERIC_READ,
								FILE_SHARE_READ,
								null,
								OPEN_EXISTING,
								FILE_ATTRIBUTE_NORMAL,
								null);

			//	開けているか確認
			if( _hFile == INVALID_HANDLE_VALUE )
			{
				char cTemp[256];
				sprintf(cTemp, "ファイルの読み込みに失敗しました(%s)", pPath);
				Message(cTemp, "エラー");
				return null;
			}

			dword dum;
			//	サイズを取得し、データの受け皿を用意する
			_dwSize = GetFileSize(_hFile, null);
			_pBuffer = new byte[_dwSize];
			//	データの読み込み
			ReadFile(_hFile, _pBuffer, _dwSize, &dum, null);

			CloseHandle(_hFile);

			return _pBuffer;
		}

		//---------------------------------------------------------------------------
		//	参照して読み込み
		//!	@return ファイルデータ
		//---------------------------------------------------------------------------
		void*	File::Read(void)
		{
			if( ReadReference() )
				return Read(_cPath);

			return null;
		}

		//---------------------------------------------------------------------------
		//	書き込み
		//!	@param pPath [in] パス
		//!	@param pBuffer [in] データ
		//!	@param iSize [in] サイズ
		//---------------------------------------------------------------------------
		void	File::Write(const char* pPath, const void* pBuffer, int iSize)
		{
			CloseCheck();

			//---------------------------------------------------------------------------
			//	ファイルを作成する(存在すれば上書きする)
			//---------------------------------------------------------------------------
			_hFile = CreateFile(pPath,
								GENERIC_WRITE,
								FILE_SHARE_WRITE,
								null,
								CREATE_ALWAYS,
								FILE_ATTRIBUTE_NORMAL,
								null);

			//	ファイルを作成できたか確認
			if( _hFile == INVALID_HANDLE_VALUE )
			{
				char cTemp[256];
				sprintf(cTemp, "ファイルの書き込みに失敗しました(%s)", pPath);
				Message(cTemp, "エラー");
				return;
			}

			//	ファイルの先頭に行く
			SetFilePointer(_hFile, 0, null, FILE_END);

			dword dum;
			//	データを書き出す
			WriteFile(_hFile, pBuffer, iSize, &dum, null);

			CloseHandle(_hFile);
		}

		//---------------------------------------------------------------------------
		//	ファイルの参照して読み込む
		//!	@param pBuffer [in] データ
		//!	@param isize [in] サイズ
		//---------------------------------------------------------------------------
		void	File::Write(const void* pBuffer, int iSize)
		{
			if( WriteReference() )
				Write(_cPath, pBuffer, iSize);
		}

		//---------------------------------------------------------------------------
		//	読み込み
		//!	@param pPath [in] パス
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	File::ReadingFile(const char* pPath)
		{
			CloseCheck();
			strcpy(_cPath, pPath);

			//---------------------------------------------------------------------------
			//	ファイルを開く
			//---------------------------------------------------------------------------
			_hFile = CreateFile(pPath,
								GENERIC_READ,
								FILE_SHARE_READ,
								null,
								OPEN_EXISTING,
								FILE_ATTRIBUTE_NORMAL,
								null);

			//	開けているか確認
			if( _hFile == INVALID_HANDLE_VALUE )
			{
				char cTemp[256];
				sprintf(cTemp, "ファイルの読み込みに失敗しました(%s)", pPath);
				Message(cTemp, "エラー");
				return false;
			}

			_bOpen = true;

			return true;
		}

		//---------------------------------------------------------------------------
		//	読み込む
		//!	@param pBuffer [out] データの受け皿
		//!	@param iSize [in] サイズ
		//---------------------------------------------------------------------------
		void	File::Reading(void* pBuffer, int iSize)
		{
			dword dum;
			ReadFile(_hFile, pBuffer, iSize, &dum, null);
		}

		//---------------------------------------------------------------------------
		//	読み込み
		//!	@param pPath [in] パス
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	File::WrittingFile(const char* pPath)
		{
			CloseCheck();
			strcpy(_cPath, pPath);

			//---------------------------------------------------------------------------
			//	ファイルを作成する(存在すれば上書きする)
			//---------------------------------------------------------------------------
			_hFile = CreateFile(pPath,
								GENERIC_WRITE,
								FILE_SHARE_WRITE,
								null,
								CREATE_ALWAYS,
								FILE_ATTRIBUTE_NORMAL,
								null);

			//	ファイルを作成できたか確認
			if( _hFile == INVALID_HANDLE_VALUE )
			{
				char cTemp[256];
				sprintf(cTemp, "ファイルの書き込みに失敗しました(%s)", pPath);
				Message(cTemp, "エラー");
				return false;
			}
			//	ファイルの先頭に行く
			SetFilePointer(_hFile, 0, null, FILE_END);

			_bOpen = true;

			return true;
		}

		//---------------------------------------------------------------------------
		//	書き込む
		//!	@param pBuffer [in] 読み込むデータ
		//!	@param iSize [in] 読み込むデータサイズ
		//---------------------------------------------------------------------------
		void	File::Writting(const void* pBuffer, int iSize)
		{
			dword dum;
			WriteFile(_hFile, pBuffer, iSize, &dum, null);
		}
		
		//---------------------------------------------------------------------------
		//	参照時に使用するオリジナルフォーマットの設定
		//!	@param pName [in] ファイル名(説明)
		//! @param pExtension [in] 拡張子名
		//---------------------------------------------------------------------------
		bool	File::SetOrigin(const char* pName, const char* pExtension)
		{
			if( _dwOriginNum >= 32 ) return false;

			char cTemp[256];
			sprintf(cTemp, "%s(.%s)", pName, pExtension);
			strcpy(_cName[_dwOriginNum], cTemp);
			sprintf(cTemp, "*.%s", pExtension);
			strcpy(_cExtension[_dwOriginNum], cTemp);

			_dwOriginNum++;

			return true;
		}

		//---------------------------------------------------------------------------
		//	オリジナルフォーマットのリセット
		//---------------------------------------------------------------------------
		void	File::ResetOriin(void)
		{
			ZeroMemory(_cName, sizeof(_cName));
			ZeroMemory(_cExtension, sizeof(_cExtension));
			_dwOriginNum = 0;
		}

		//---------------------------------------------------------------------------
		//!	参照時の初期ディレクトリー設定
		//!	@param pDirectory [in] ディレクトリー
		//---------------------------------------------------------------------------
		void	File::SetDeirectory(char* pDirectory)
		{
			strcpy(_cDirectory, pDirectory);
		}

		//---------------------------------------------------------------------------
		//!	参照可能なファイルの設定
		//!	@param iFormat [in] フォーマット
		//---------------------------------------------------------------------------
		void	File::SetReferenceFormat(int iFormat)
		{
			_iFormat = iFormat;
		}

		//---------------------------------------------------------------------------
		//!	読み込み中のデータを取得
		//!	@return データ
		//---------------------------------------------------------------------------
		void*	File::GetFile(void)
		{
			return _pBuffer;
		}

		//---------------------------------------------------------------------------
		//!	読み込み中のデータサイズ取得
		//!	@return データサイズ
		//---------------------------------------------------------------------------
		int		File::GetSize(void)
		{
			return _dwSize;
		}

		//---------------------------------------------------------------------------
		//!	参照時の初期ディレクトリー取得
		//!	@return ディレクトリー
		//---------------------------------------------------------------------------
		char*	File::GetDeirectory(void)
		{
			return _cDirectory;
		}

		//---------------------------------------------------------------------------
		//!	参照したファイル名取得
		//!	@return ファイル名
		//---------------------------------------------------------------------------
		char*	File::GetFilePath(void)
		{
			return _cPath;
		}

		//---------------------------------------------------------------------------
		//!	参照可能なファイルの取得
		//!	@return フォーマット
		//---------------------------------------------------------------------------
		int		File::GetReferenceFormat(void)
		{
			return _iFormat;
		}

		//---------------------------------------------------------------------------
		//	参照(読み込み)
		//!	@return パス
		//---------------------------------------------------------------------------
		char*	File::ReadReference(void)
		{
			//---------------------------------------------------------------------------
			//	ファイルの参照
			//---------------------------------------------------------------------------
			OPENFILENAME ofn;
			char cFileName[256];
			cFileName[0] = '\0';

			char cFormat[256];
			GetReference(cFormat);

			ZeroMemory(&ofn, sizeof(OPENFILENAME));
			ofn.lStructSize		= sizeof(OPENFILENAME);
			ofn.hwndOwner		= null;
			ofn.lpstrFilter		= cFormat;
			ofn.lpstrFile		= cFileName;
			ofn.lpstrFileTitle	= null;
			ofn.lpstrInitialDir = _cDirectory;
			ofn.nMaxFile		= 256;
			ofn.Flags			= OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
			ofn.lpstrDefExt		= "";
			ofn.lpstrTitle		= "ファイルを開く";
			if( GetOpenFileName(&ofn) == 0 )
				return null;

			strcpy(_cPath, cFileName);
			return _cPath;
		}

		//---------------------------------------------------------------------------
		//	参照(書き込み)
		//!	@return パス
		//---------------------------------------------------------------------------
		char*	File::WriteReference(void)
		{
			//---------------------------------------------------------------------------
			//	ファイルの参照
			//---------------------------------------------------------------------------
			OPENFILENAME ofn;
			char cFileName[256];
			cFileName[0] = '\0';

			char cFormat[256];
			GetReference(cFormat);

			memset(&ofn, 0, sizeof(OPENFILENAME));
			ofn.lStructSize			= sizeof(OPENFILENAME);
			ofn.hwndOwner			= null;
			ofn.lpstrFilter			= cFormat;
			ofn.lpstrFile			= cFileName;	
			ofn.nMaxFile			= 256;
			ofn.nFilterIndex		= 0;
			ofn.lpstrCustomFilter	= cFormat;
			ofn.nMaxCustFilter		= 256;
			ofn.lpstrFileTitle		= null;
			ofn.lpstrInitialDir		= _cDirectory;
			ofn.Flags				= OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_CREATEPROMPT;
			ofn.lpstrDefExt			= "";
			ofn.lpstrTitle			= "ファイルの保存";
			if( GetSaveFileName(&ofn) == 0 )
				return null;

			strcpy(_cPath, cFileName);
			return _cPath;
		}

		//---------------------------------------------------------------------------
		//	閉じる
		//---------------------------------------------------------------------------
		void	File::CloseFile(void)
		{
			if( _bOpen )
				CloseHandle(_hFile);

			_bOpen = false;
		}

		//---------------------------------------------------------------------------
		//	ファイルの閉じ忘れを確認
		//---------------------------------------------------------------------------
		void	File::CloseCheck(void)
		{
			if( _bOpen )
				Message(CharSet::Format("ファイルが閉じられていません(%s)", _cPath), "警告");

			CloseFile();
		}

		//---------------------------------------------------------------------------
		//	参照可能なファイルの取得
		//!	@param pFormat [out] 参照用文字列
		//---------------------------------------------------------------------------
		void	File::GetReference(char* pFormat)
		{
			dword dwPointer = 0;

			//	Bitmap
			if( _iFormat & BMP )
			{
				strcpy(&pFormat[dwPointer], "Bitmap(*.bmp)");
				dwPointer += (dword)strlen(&pFormat[dwPointer]) + 1;
				strcpy(&pFormat[dwPointer], "*.bmp");
				dwPointer += (dword)strlen(&pFormat[dwPointer]) + 1;
			}
			//	JPEG
			if( _iFormat & JPG )
			{
				strcpy(&pFormat[dwPointer], "JPEG(*.jpeg,*.jpg)");
				dwPointer += (dword)strlen(&pFormat[dwPointer]) + 1;
				strcpy(&pFormat[dwPointer], "*.jpeg,*.jpg");
				dwPointer += (dword)strlen(&pFormat[dwPointer]) + 1;
			}
			//	PNG
			if( _iFormat & PNG )
			{
				strcpy(&pFormat[dwPointer], "PNG(*.png)");
				dwPointer += (dword)strlen(&pFormat[dwPointer]) + 1;
				strcpy(&pFormat[dwPointer], "*.png");
				dwPointer += (dword)strlen(&pFormat[dwPointer]) + 1;
			}
			//	Text
			if( _iFormat & TXT )
			{
				strcpy(&pFormat[dwPointer], "Text(*.txt)");
				dwPointer += (dword)strlen(&pFormat[dwPointer]) + 1;
				strcpy(&pFormat[dwPointer], "*.txt");
				dwPointer += (dword)strlen(&pFormat[dwPointer]) + 1;
			}

			//	Original
			if( _iFormat & ORIGIN )
			{
				for(dword i=0; i < _dwOriginNum; i++)
				{
					strcpy(&pFormat[dwPointer], _cName[i]);
					dwPointer += (dword)strlen(&pFormat[dwPointer]) + 1;
					strcpy(&pFormat[dwPointer], _cExtension[i]);
					dwPointer += (dword)strlen(&pFormat[dwPointer]) + 1;
				}
			}

			//	All Files
			if( _iFormat & ALL )
			{
				strcpy(&pFormat[dwPointer], "All Files(*.*)");
				dwPointer += (dword)strlen(&pFormat[dwPointer]) + 1;
				strcpy(&pFormat[dwPointer], "*.*");
				dwPointer += (dword)strlen(&pFormat[dwPointer]) + 1;
			}

			pFormat[dwPointer] = 0;
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================