//---------------------------------------------------------------------------
//!
//!	@file	Lib.MeshManager.Mesh.cpp
//!	@brief	メッシュ管理
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//	include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace MeshManager
	{
		//---------------------------------------------------------------------------
		//	描画
		//!	@param pShader [in] シェーダー
		//!	@param pTechnique [in] テクニック
		//---------------------------------------------------------------------------
		void	Mesh::Render(Shader::Shader* pShader, char* pTechnique, dword dwPass)
		{
			pShader->SetTechnique(pTechnique);
			pShader->BeginShader();
			pShader->BeginPass(dwPass);

			pShader->SetMatrix("mWorld", _TransMatrix);

			for(dword i=0; i < _dwMaterialNum; i++)
			{
				pShader->SetTexture("DiffuseMap", _pMaterial[i].pTexture);
				pShader->SetTexture("NormalMap", _pMaterial[i].pNormal);
				pShader->SetFloatArray("Diffuse", (float*)&_pMaterial[i].Diffuse, 4);
				pShader->SetFloatArray("Ambient", (float*)&_pMaterial[i].Ambient, 3);
				pShader->CommitChanges();

				//	描画
				_pMesh->DrawSubset(i);
			}

			pShader->EndPass();
			pShader->EndShader();
		}

		//---------------------------------------------------------------------------
		//	描画
		//!	@param i [in] 材質番号
		//---------------------------------------------------------------------------
		void	Mesh::Render(dword i)
		{
			_pMesh->DrawSubset(i);
		}

		//---------------------------------------------------------------------------
		//	UVアニメーション
		//!	@param fU [in] Uの移動量
		//!	@param fV [in] Vの移動量
		//---------------------------------------------------------------------------
		void	Mesh::UVAnimation(float fU, float fV)
		{
			Vertex* pVertex;
			dword dwVertexNum = _pMesh->GetNumVertices();

			_pMesh->LockVertexBuffer(D3DLOCK_DISCARD, (void**)&pVertex);
			for(dword i=0; i < dwVertexNum; i++)
			{
				pVertex[i].UV.x += fU;
				pVertex[i].UV.y += fV;
			}
			_pMesh->UnlockVertexBuffer();
		}

		//---------------------------------------------------------------------------
		//	UVアニメーション
		//!	@param pOut [out] 衝突した位置
		//!	@param Pos [in] 開始位置
		//!	@param pVec [in/out] 向き/衝突した面の法線
		//!	@param pDist [in/out] 距離/衝突した位置までの距離
		//! @param TransMatrix [in] 変換行列
		//!	@return 交差した面番号
		//---------------------------------------------------------------------------
		int		Mesh::RayPick(Vector3* pOut, const Vector3& Pos, Vector3* pVec, float* pDist, const Matrix& TransMatrix)
		{
			int iRet = -1;
			dword i, a, b, c;
			float fNear, t;

			float* pVertexs;
			word* pIndex;
			Vector3 Vec, Normal;
			Vector3 p, v1, v2, v3;

			Math::Normalize(&Vec, *pVec);

			//	適当に大きな値を入れておく
			fNear = 1000000.0f;

			float fDist= *pDist * *pDist;

			//	FVFより頂点サイズを求める
			dword dwFVF = _pMesh->GetFVF();
			dword dwVertexSize = D3DXGetFVFVertexSize(dwFVF) / sizeof(float);

			dword dwFaceNum = _pMesh->GetNumFaces();

			//	頂点情報と面の頂点番号を取得する
			_pMesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVertexs);
			_pMesh->LockIndexBuffer(D3DLOCK_READONLY,  (void**)&pIndex);

			//	面数だけ比較し調べる
			for(i = 0; i < dwFaceNum; i++)
			{
				//	面の頂点番号を取得する
				a = pIndex[i * 3 + 0];
				b = pIndex[i * 3 + 1];
				c = pIndex[i * 3 + 2];

				//	頂点の座標を取得する
				Vector3 p1(pVertexs[a * dwVertexSize], pVertexs[a * dwVertexSize + 1], pVertexs[a * dwVertexSize + 2]);
				Vector3 p2(pVertexs[b * dwVertexSize], pVertexs[b * dwVertexSize + 1], pVertexs[b * dwVertexSize + 2]);
				Vector3 p3(pVertexs[c * dwVertexSize], pVertexs[c * dwVertexSize + 1], pVertexs[c * dwVertexSize + 2]);
				D3DXVec3TransformCoord(&p1, &p1, &TransMatrix);
				D3DXVec3TransformCoord(&p2, &p2, &TransMatrix);
				D3DXVec3TransformCoord(&p3, &p3, &TransMatrix);

				//	面との距離判定
				if( Math::Length2(((p1 + p2 + p3) / 3.0f) - Pos) > fDist )
					continue;

				//	外積で法線を算出
				Math::Cross(&Normal, (p2 - p1), (p3 - p1));
				//	内積により面の向きを確認
				if( Math::Dot(Normal, Vec) > 0 )
					continue;

				//	面との交点算出
				//	交点p = pos + t * vec・・・�@
				//	(p1 - p)・n = 0・・・�A
				//	p・n = pos・n + t * vec・n・・・�B = �@ ・ n
				//	p1・n = pos・n + t * vec・n・・・�C = �B - �A
				//	t = (p1 - pos)・n / (vec・n)・・・�Cをtについて解く
				t = Math::Dot(p1 - Pos, Normal) / Math::Dot(Vec, Normal);
				if( t < 0 || t > fNear )
					continue;
				p = Pos + t * Vec;

				//	交点が面の内側にあるか確認
				Math::Cross(&v1, p1 - p, p2 - p1);
				if( Math::Dot(v1, Normal) < 0 )
					continue;
				Math::Cross(&v2, p2 - p, p3 - p2);
				if( Math::Dot(v2, Normal) < 0 )
					continue;
				Math::Cross(&v3, p3 - p, p1 - p3);
				if( Math::Dot(v3, Normal) < 0 )
					continue;

				//	交差した点と面の番号、距離を保存する
				*pOut = p;
				*pVec = Normal;
				iRet = i;
				fNear = t;
			}
			//	頂点情報と面の頂点番号の取得終了
			_pMesh->UnlockIndexBuffer();
			_pMesh->UnlockVertexBuffer();

			*pDist = fNear;

			return iRet;
		}

		//---------------------------------------------------------------------------
		//	メッシュの取得
		//!	@return メッシュ
		//---------------------------------------------------------------------------
		LPD3DXMESH	Mesh::GetMesh(void)
		{
			return _pMesh;
		}

		//---------------------------------------------------------------------------
		//	転送行列のセット
		//!	@param TransMatrix 転送行列
		//---------------------------------------------------------------------------
		void	Mesh::SetTransMatrix(const Matrix& TransMatrix)
		{
			_TransMatrix = TransMatrix;
		}

		//---------------------------------------------------------------------------
		//	転送行列の取得
		//!	@return 転送行列
		//---------------------------------------------------------------------------
		Matrix	Mesh::GetTransMatrix(void)
		{
			return _TransMatrix;
		}

		//---------------------------------------------------------------------------
		//	材質数の取得
		//!	@return 材質数
		//---------------------------------------------------------------------------
		dword	Mesh::GetMaterialNum(void)
		{
			return _dwMaterialNum;
		}

		//---------------------------------------------------------------------------
		//	材質の取得
		//!	@return 材質
		//---------------------------------------------------------------------------
		Material*	Mesh::GetMaterial(void)
		{
			return _pMaterial;
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================