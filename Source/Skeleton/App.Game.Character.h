//---------------------------------------------------------------------------
//!
//!	@file	App.Game.Character.h
//!	@brief	キャラクタークラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

// --------------------------------------------------------------------------
//	include
// --------------------------------------------------------------------------
#include "App.h"

namespace App
{
	namespace Game
	{
		//===========================================================================
		//!	キャラクタークラス
		//===========================================================================
		class Character
		{
		public:
			//!	コンストラクタ
			Character(void);
			//!	デストラクタ
			virtual ~Character(void);

			//!	初期化
			virtual bool	Init(void) = 0;
			//!	解放
			virtual	void	Cleanup(void) = 0;

			//!	更新
			virtual	void	Update(void) = 0;
			//!	描画
			virtual	void	Render(Lib::Shader::Shader* pShader) = 0;

			//!	位置取得
			Lib::Vector3	GetPosition(void);
			//!	ライフ取得
			int		GetLife(void);
			//!	モード取得
			int		GetMode(void);

		protected:
			Lib::MeshManager::SkinMesh*	_pSkinMesh;	//!< スキンメッシュ
			App::Collision::Object		_Collision;	//!< 当たり判定

			float			_fScale;	//!< スケール
			Lib::Vector3	_Pos;		//!< 位置
			int				_iLife;		//!< ライフ
			int				_iMode;		//!< モード
			float			_fAngle;	//!< 向き
		};
	}
}

//============================================================================
//	END OF FILE
//============================================================================