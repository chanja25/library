//---------------------------------------------------------------------------
//!
//!	@file	Lib.InputManager.h
//!	@brief	入力関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#pragma once

//---------------------------------------------------------------------------
// include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace InputManager
	{
		//!	スティックの遊び
		const float AXIS_MARGIN = 0.25f;

		//!	ジョイスティックの数を取得
		dword	GetJoyNum(void);

		//===========================================================================
		//!	Keyboradクラス
		//===========================================================================
		class Keyboard
		{
		public:
			//!	コンストラクタ
			Keyboard(void);
			//!	デストラクタ
			virtual ~Keyboard(void);

			//!	キーが押されているかチェック
			virtual bool	IsKeyPress(int iKey);
			//!	キーを押したかチェック
			virtual bool	IsKeyPush(int iKey);
			//!	キーが離されたかチェック
			virtual bool	IsKeyRelease(int iKey);
		};

		//===========================================================================
		//!	Mouseクラス
		//===========================================================================
		class Mouse
			: public Keyboard
		{
		public:
			//!	コンストラクタ
			Mouse(void);
			//!	デストラクタ
			~Mouse(void);

			//!	キーが押されているかチェック
			bool	IsKeyPress(int iKey);
			//!	キーを押したかチェック
			bool	IsKeyPush(int iKey);
			//!	キーが離されたかチェック
			bool	IsKeyRelease(int iKey);

			//!	X取得
			int		GetX(void);
			//!	Y取得
			int		GetY(void);
			//!	ホイール取得
			int		GetWheel(void);
		};

		//===========================================================================
		//!	JoyStickクラス
		//===========================================================================
		class JoyStick
			: public Keyboard
		{
		public:
			//!	コンストラクタ
			JoyStick(void);
			//!	デストラクタ
			~JoyStick(void);

			//!	キーが押されているかチェック
			bool	IsKeyPress(int iKey);
			//!	キーを押したかチェック
			bool	IsKeyPush(int iKey);
			//!	キーが離されたかチェック
			bool	IsKeyRelease(int iKey);

			//!	左スティックの状態取得
			Vector2	GetAxisLeft(void);
			//!	右スティックの状態取得
			Vector2	GetAxisRight(void);

			//!	ジョイスティック番号の設定
			void	SetNo(dword dwNo);
			//!	ジョイスティック番号の取得
			dword	GetNo(void);

		private:
			dword	_dwNo;	//!< ジョイスティックの番号
		};
	}
}

//============================================================================
//	END OF FILE
//============================================================================