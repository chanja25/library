//---------------------------------------------------------------------------
//!
//!	@file	Lib.CharSet.cpp
//!	@brief	文字関連
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//	include
//---------------------------------------------------------------------------
#include "Lib.h"

namespace Lib
{
	namespace CharSet
	{
		//---------------------------------------------------------------------------
		//	文字列生成
		//!	@param pString [in] 表示する文字
		//!	@param ... [in] printfの様に使う。例：("%d", 100)
		//!	@return 生成した文字列
		//---------------------------------------------------------------------------
		char*	Format(const char* pString, ...)
		{
			//	printf("%s" ,a)のように文字列生成
			static va_list list;
			static char cTemp[256];
			va_start(list, pString);
			vsprintf(cTemp, pString, list);
			va_end(list);

			return cTemp;
		}

		//---------------------------------------------------------------------------
		//	数値判断
		//!	@param cToken [in] トークン
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Number(char cToken)
		{
			if( cToken >= '0' && cToken <= '9' ||
				cToken == '.' )
				return true;

			return false;
		}

		//---------------------------------------------------------------------------
		//	文字判断
		//!	@param cToken [in] トークン
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	String(char cToken)
		{
			if( (cToken >= 'a' && cToken <= 'z') ||
				(cToken >= 'A' && cToken <= 'Z') )
				return true;

			return false;
		}

		//---------------------------------------------------------------------------
		//	演算子判断
		//!	@param cToken [in] トークン
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Oparetor(char cToken)
		{
			if( cToken == '+' || cToken == '-' ||
				cToken == '*' || cToken == '/' ||
				cToken == '%' || cToken == '=' ||
				cToken == '!' || cToken == '^' ||
				cToken == '&' || cToken == '|' )
				return true;

			return false;
		}

		//---------------------------------------------------------------------------
		//	終了判断
		//!	@param cToken [in] トークン
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	End(char cToken)
		{
			if(	cToken == '(' || cToken == ')' ||
				cToken == '{' || cToken == '}' ||
				cToken == '[' || cToken == ']' ||
				cToken == '<' || cToken == '>' ||
				cToken == ' ' || cToken == ';' ||
				cToken == ',' || cToken == '\t' ||
				cToken == '\r' || cToken == '\n' ||
				cToken == '\0' )
				return true;

			return false;
		}

		//---------------------------------------------------------------------------
		//	無視判断
		//!	@param cToken [in] トークン
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Through(char cToken)
		{
			if( cToken == ' ' || cToken == ';' ||
				cToken == ',' || cToken == '\t' ||
				cToken == '\r' || cToken == '\n' ||
				cToken == '\0' )
				return true;

			return false;
		}

		//---------------------------------------------------------------------------
		//	コンストラクタ
		//---------------------------------------------------------------------------
		Lexer::Lexer(void)
			: _pBuffer(null), _dwPointer(0)
		{
		}

		//---------------------------------------------------------------------------
		//	デストラクタ
		//---------------------------------------------------------------------------
		Lexer::~Lexer(void)
		{
			SafeDelete(_pBuffer);
		}

		//---------------------------------------------------------------------------
		//	解析するデータの読み込み
		//!	@param pFileName [in] ファイル名
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Lexer::LoadBuffer(const char* pFileName)
		{
			Lib::FileIO::File File;
			if( !File.Read(pFileName) )
				return false;

			_dwSize = File.GetSize();
			_pBuffer = new char[_dwSize + 1];
			_pBuffer[_dwSize] = '\0';
			memcpy(_pBuffer, File.GetFile(), File.GetSize());
			return true;
		}

		//---------------------------------------------------------------------------
		//	解析するデータのセット
		//!	@param pBuffer [in] データ
		//---------------------------------------------------------------------------
		void	Lexer::SetBuffer(const char* pBuffer)
		{
			_pBuffer = new char[strlen(pBuffer) + 1];
			strcpy(_pBuffer, pBuffer);
		}

		//---------------------------------------------------------------------------
		//	トークン取得
		//!	@return トークン
		//---------------------------------------------------------------------------
		char*	Lexer::Pop(void)
		{
			//	終了
			if( _dwPointer >= _dwSize )
				return "Eof";

			ZeroMemory(_cToken, sizeof(_cToken));
			char cToken = _pBuffer[_dwPointer];
			dword dwPointer = 0;

			//	トークンの開始まで進める
			while( true )
			{
				//	コメント
				if( cToken == '/' )
				{
					//	(//)
					if( _pBuffer[_dwPointer + 1] == '/')
					{
						_dwPointer++;
						while( cToken != '\n' )
							cToken = _pBuffer[++_dwPointer];
					}
					//	(/**/)
					if( _pBuffer[_dwPointer + 1] == '*' )
					{
						_dwPointer++;
						while( true )
						{
							cToken = _pBuffer[++_dwPointer];
							if( cToken == '*' &&
								_pBuffer[_dwPointer + 1] == '/' )
								break;
						}
						_dwPointer++;
					}
				}
				//	スルー
				if( !Through(cToken) )
					break;
				//	終了
				if( ++_dwPointer >= _dwSize )
					return "Eof";
				cToken = _pBuffer[_dwPointer];
			}

			//	文字列
			if( cToken == '\"')
			{
				cToken = _pBuffer[++_dwPointer];
				while( cToken != '\"' )
				{
					_cToken[dwPointer++] = cToken;
					cToken = _pBuffer[++_dwPointer];
				}
				_dwPointer++;
				return _cToken;
			}

			//	数値(16進数)
			if( cToken == '0' )
			{
				_cToken[dwPointer++] = cToken;
				cToken = _pBuffer[++_dwPointer];
				if( !Number(cToken) && cToken != 'x' )
					return "Err";

				while( Number(cToken) || 
					 ( cToken >= 'a' && cToken <= 'f') ||
					 ( cToken >= 'A' && cToken <= 'F') )
				{
					_cToken[dwPointer++] = cToken;
					cToken = _pBuffer[++_dwPointer];
				}
				return _cToken;
			}

			//	数値
			if( Number(cToken) ||
				(cToken == '-' && Number(_pBuffer[_dwPointer + 1])) )
			{
				do
				{
					_cToken[dwPointer++] = cToken;
					cToken = _pBuffer[++_dwPointer];
				}
				while( Number(cToken) );
				if( String(cToken) )
					return "Err";

				return _cToken;
			}

			//	文字
			if( String(cToken) )
			{
				do
				{
					_cToken[dwPointer++] = cToken;
					cToken = _pBuffer[++_dwPointer];
					if( Oparetor(cToken) )
						break;
				}
				while( !End(cToken) );

				return _cToken;
			}

			//	演算子
			if( Oparetor(cToken) )
			{
				_cToken[dwPointer++] = cToken;
				_dwPointer++;
				return _cToken;
			}

			//	その他
			_cToken[dwPointer] = cToken;
			_dwPointer++;
			return _cToken;
		}

		//---------------------------------------------------------------------------
		//!	指定されたトークンを探す
		//!	@param pString [in] データ
		//!	@return 結果
		//---------------------------------------------------------------------------
		char*	Lexer::Peek(void)
		{
			dword dwPointer = _dwPointer;
			strcpy(_cPeek, Pop());
			_dwPointer = dwPointer;
			return _cPeek;
		}


		//---------------------------------------------------------------------------
		//!	指定されたトークンを探す
		//!	@param pString [in] データ
		//!	@return 結果
		//---------------------------------------------------------------------------
		bool	Lexer::Seek(const char* pString)
		{
			dword dwPointer = _dwPointer;
			while( strcmp(pString, Pop()) )
			{
				if( _dwPointer == _dwSize )
				{
					_dwPointer = dwPointer;
					return false;
				}
			}
			return true;
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================